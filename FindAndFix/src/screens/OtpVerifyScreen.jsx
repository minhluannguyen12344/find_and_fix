import { Text, View, Image, SafeAreaView, Keyboard } from "react-native";
import { OtpInput } from "react-native-otp-entry";
import app_icon from "../../asset/imgs/app_icon.png";
import { useForm, Controller } from "react-hook-form";
import React, { useState } from "react";
import { Button, Icon } from "@rneui/themed";
import localHostEnv from "../config/localhost_env";
import Toast from "react-native-toast-message";
import axios from "axios";
import { useFonts } from "expo-font";

const OtpVerifyScreen = ({ route, navigation }) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const { email, password, fullname, role, address, phonenumber, avatar } =
    route.params;
  const [otp, setOtp] = useState("");
  const [method_id, setMethod_id] = useState("");
  //   console.log(otp);
  const showSuccessToast = () => {
    Toast.show({
      type: "success",
      visibilityTime: 800,
      text1: "Tạo tài khoản thành công",
    });
  };
  const showErrorToast = () => {
    Toast.show({
      type: "error",
      visibilityTime: 800,
      text1: "Tạo tài khoản thất bại",
    });
  };
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({});
  const onSubmit = async (data) => {
    console.log(data.otp);
    const formData = new FormData();
    formData.append("email", email);
    formData.append("password", password);
    formData.append("fullname", fullname);
    formData.append("role", role);
    formData.append("address", address);
    formData.append("phonenumber", phonenumber);
    formData.append("image", {
      uri: avatar,
      type: "image",
      name: avatar.split("/").pop(),
    });
    console.log(method_id, data.otp);
    try {
      const response = await axios.post(`${localHostEnv}/verify-otp`, {
        method_id: method_id,
        code: data.otp,
      });
      console.log(response.data);
      const axiosResponse = await axios.post(
        `${localHostEnv}/auth/signup`,
        formData,
        {
          headers: {
            Accept: "application/json",
          },
        }
      );
      setTimeout(() => {
        navigation.navigate("LoginScreen");
      }, 1000);
      showSuccessToast();
      setTimeout(() => {
        navigation.navigate("LoginScreen");
      }, 1000);
    } catch (error) {
      showErrorToast();
      console.log(error);
    }
  };
  const handleSendOtp = async () => {
    try {
      const response = await axios.post(`${localHostEnv}/send-otp`, {
        email: email,
      });
      console.log(response.data);
      setMethod_id(response.data.data.email_id);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <SafeAreaView
      onTouchStart={() => Keyboard.dismiss()}
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: "white",
        height: "100%",
      }}
    >
      <Text
        style={{
          fontSize: 18,
          fontFamily: "Roboto-Bold",
          textAlign: "start",
          width: 320,
          color: "#496989",
          marginTop: 50,
          marginLeft: -20,
        }}
      >
        Mã OTP đã được gửi đến {email} của bạn để xác thực
      </Text>
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <OtpInput
            numberOfDigits={6}
            onTextChange={onChange}
            onFilled={() => setOtp(value)}
            value={otp}
            theme={{
              pinCodeContainerStyle: {
                width: 40,
                height: 40,
                borderColor: "#57A6A1",
                borderWidth: 2,
              },
              containerStyle: {
                width: 350,
                marginTop: 50,
              },
            }}
          />
        )}
        name="otp"
        rules={{ required: true, minLength: 6, maxLength: 6 }}
      />

      {errors.otp && (
        <Text style={{ color: "red", marginTop: 20 }}>
          Mã OTP cần 6 số để xác minh
        </Text>
      )}
      <Button
        title="Gửi Otp"
        onPress={handleSendOtp}
        buttonStyle={{
          backgroundColor: "#57A6A1",
          width: 200,
          height: 68,
          borderRadius: 12,
          marginTop: 150,
          boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
        }}
        titleStyle={{
          fontSize: 20,
          fontWeight: "bold",
        }}
      />
      <Button
        title="Xác Minh"
        onPress={handleSubmit(onSubmit)}
        buttonStyle={{
          backgroundColor: "white",
          width: 200,
          height: 68,
          borderRadius: 12,

          borderColor: "#57A6A1",
          borderWidth: 1,
          marginBottom: 20,
          marginTop: 20,
        }}
        titleStyle={{
          fontSize: 20,
          color: "#57A6A1",
          fontFamily: "Roboto-Bold",
        }}
      />
      <View
        style={{
          position: "relative",
          bottom: 0,
          top: 50,
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text
          style={{
            color: "#496989",
            marginTop: 20,
            fontSize: 20,
            fontFamily: "Roboto",
          }}
          onPress={() =>
            navigation.navigate("SignUpScreen", {
              role: role,
            })
          }
        >
          Quay về trang đăng ký
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default OtpVerifyScreen;
