import React from "react";
import { useFonts } from "expo-font";
import { useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import {
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  Keyboard,
} from "react-native";
import localHostEnv from "../../config/localhost_env";
import { Header, Icon, Button } from "@rneui/themed";
import emty_search from "../../../asset/imgs/empty_search.jpg";
const SearchScreen = () => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const userInfo = useSelector((state) => state.user.userInfo);
  const navigation = useNavigation();
  const [searchName, setSearchName] = useState("");
  console.log(searchName);
  const [area, setArea] = useState();
  const [searchData, setSearchData] = useState([]);
  const getSearchData = async () => {
    try {
      const response = await axios.get(
        `${localHostEnv}/services?search=${searchName}`
      );

      setSearchData(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    if (searchName !== "") {
      getSearchData();
    }
  }, [searchName]);
  return (
    <View
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: "#EEEDEB",
        height: "100%",
      }}
      onTouchStart={() => Keyboard.dismiss()}
    >
      <Header
        backgroundColor="#57A6A1"
        containerStyle={{
          height: 100,
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: 5,
        }}
        leftComponent={
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Icon
              type="font-awnsome-5"
              name="chevron-left"
              color="white"
              size={50}
              containerStyle={{
                marginTop: -10,
              }}
              onPress={() => navigation.goBack()}
            />
          </View>
        }
        centerComponent={
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: 20,
                fontFamily: "Roboto-bold",
              }}
            >
              Tiềm kiếm dịch vụ
            </Text>
          </View>
        }
      />
      <TextInput
        placeholder="Tìm kiếm theo tên dịch vụ. Ví dụ: Sửa điện, sửa chữa..."
        onChangeText={(text) => setSearchName(text)}
        placeholderTextColor="#496989"
        style={{
          width: "95%",
          height: 50,
          fontFamily: "Roboto",
          color: "#496989",
          backgroundColor: "white",
          borderRadius: 10,
          marginTop: 10,
          paddingLeft: 10,
          fontFamily: "Roboto",
        }}
      />
      <ScrollView
        contentContainerStyle={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          gap: 10,
        }}
        style={{
          width: "95%",
          marginTop: 10,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          gap: 8,
          backgroundColor: "white",
          padding: 10,
        }}
      >
        {searchName !== "" ? (
          searchData.map((item, index) => (
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "flex-start",
                backgroundColor: "#FCF8F3",
                borderRadius: 10,
                width: "100%",

                gap: 10,
              }}
              key={index}
            >
              <Image
                source={{
                  uri: `${localHostEnv}/${item.thumbnail.replace(/\\/g, "/")}`,
                }}
                style={{
                  width: 100,
                  height: 100,

                  borderTopLeftRadius: 10,
                  borderBottomLeftRadius: 10,
                }}
              />
              <View
                style={{
                  display: "flex",
                  width: "auto",
                  justifyContent: "space-between",
                  alignItems: "flex-start",
                  flexDirection: "row",
                  marginTop: 8,
                  gap: 5,
                }}
              >
                <View
                  style={{
                    display: "flex",
                    justifyContent: "flex-start",
                    alignItems: "flex-start",
                    flexDirection: "column",

                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      color: "#45474B",
                      fontFamily: "Roboto-Bold",
                      fontSize: 16,
                    }}
                  >
                    {item.servicesName}
                  </Text>
                  <View
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      gap: 5,
                    }}
                  >
                    <Icon
                      name="map-marker-alt"
                      size={15}
                      type="font-awesome-5"
                      color="#FF4C4C"
                    />
                    <Text
                      numberOfLines={2}
                      style={{
                        width: 180,
                        color: "#496989",
                        fontFamily: "Roboto-Medium",
                        fontSize: 13,
                      }}
                    >
                      {item.area}
                    </Text>
                  </View>

                  <View
                    style={{
                      díplay: "flex",
                      flexDirection: "row",
                      gap: 5,
                    }}
                  >
                    <Text
                      style={{
                        color: "#E4003A",
                        fontFamily: "Roboto-bold",
                        fontSize: 14,
                      }}
                    >
                      {item.price}
                    </Text>
                    <Text
                      style={{
                        fontFamily: "Roboto-boldItalic",
                        fontSize: 12,
                        textDecorationLine: "underline",
                        color: "#496989",
                      }}
                    >
                      đ
                    </Text>
                  </View>
                </View>
                <Text
                  style={{
                    textDecorationLine: "underline",
                    fontFamily: "Roboto-MediumItalic",
                    color: "#496989",
                    fontSize: 14,
                  }}
                  onPress={() =>
                    navigation.navigate("DetailServicesScreen", {
                      servicesId: item._id,
                      userId: userInfo._id,
                      servicesName: item.servicesName,
                      thumbnail: item.thumbnail,
                      price: item.price,
                      exp: item.exp,
                      description: item.description,
                      area: item.area,
                    })
                  }
                >
                  Chi tiết
                </Text>
              </View>
            </View>
          ))
        ) : (
          <Image
            source={emty_search}
            style={{ width: 200, height: 200, marginTop: 160 }}
          />
        )}
      </ScrollView>
    </View>
  );
};

export default SearchScreen;
