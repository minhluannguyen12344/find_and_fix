import { Text, View, Image, ScrollView } from "react-native";
import img_slider from "../../../asset/imgs/slider_intro.png";
import AppBar from "../../components/AppBar";
import { useRoute } from "@react-navigation/native";
//import component
import PoppularServices from "../../components/PoppularServices";
import Allservices from "../../components/Allservices";
import { useEffect, useState } from "react";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
//import redux
import { useDispatch, useSelector } from "react-redux";
import { updateUserInfo } from "../../config/reduxStore";
//import local hostIp
import localHostEnv from "../../config/localhost_env";
import { useFonts } from "expo-font";

const listIntroService = [
  {
    img: `${img_slider}`,
  },
  {
    img: `${img_slider}`,
  },
  {
    img: `${img_slider}`,
  },
  {
    img: `${img_slider}`,
  },
  {
    img: `${img_slider}`,
  },
];

const HomeScreen = ({ navigation }) => {
  const route = useRoute();
  const { userId } = route.params;
  const [fontsLoaded] = useFonts({
    Roboto: require("../../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  // const [user, setUser] = useState();
  const dispatch = useDispatch();

  const [services, setServices] = useState([]);
  useEffect(() => {
    const getUserInfo = async () => {
      try {
        const token = await AsyncStorage.getItem("userToken");
        const response = await axios.get(`${localHostEnv}/user/${userId}`);
        // setUser(response.data);
        dispatch(updateUserInfo(response.data));
        console.log("user", response.data);
      } catch (e) {
        console.error("Failed to fetch user data:", e);
      }
    };
    getUserInfo();
  }, [userId, dispatch]);
  useEffect(() => {
    const getServices = async () => {
      try {
        const response = await axios.get(`${localHostEnv}/services/`);
        setServices(response.data);
      } catch (e) {
        console.error("Failed to fetch services data:", e);
      }
    };
    getServices();
  }, [userId]);
  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: "#EEEDEB",
      }}
    >
      <AppBar />
      <ScrollView showsVerticalScrollIndicator={false}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 20,

              marginTop: 20,
            }}
          >
            {listIntroService.map((item, index) => (
              <Image source={item.img} key={index} />
            ))}
          </View>
        </ScrollView>

        <Text
          style={{
            color: "#45474B",
            fontFamily: "Roboto-Bold",
            fontSize: 22,
            marginLeft: 20,
            marginTop: 20,
          }}
        >
          Tất cả dịch vụ
        </Text>
        <View
          style={{
            marginBottom: 50,
          }}
        >
          <Allservices servicesList={services} />
        </View>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
