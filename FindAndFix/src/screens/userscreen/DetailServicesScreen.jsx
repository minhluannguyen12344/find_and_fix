import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
} from "react-native";
import React, { useState } from "react";
import { Header, Icon, Button } from "@rneui/themed";
import localHostEnv from "../../config/localhost_env";
import Comment from "../../components/Comment";
import { useDispatch, useSelector } from "react-redux";
import { useFonts } from "expo-font";

const DetailServicesScreen = ({ route, navigation }) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const {
    servicesId,
    providerId,
    servicesName,
    description,
    thumbnail,
    price,
    area,
    exp,
    comments,
  } = route.params;
  const userInfo = useSelector((state) => state.user.userInfo);

  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: "#EEEDEB",
      }}
    >
      <Header
        backgroundColor="#57A6A1"
        containerStyle={{
          height: 100,
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: 5,
        }}
        leftComponent={
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Icon
              type="font-awnsome-5"
              name="chevron-left"
              color="white"
              size={50}
              containerStyle={{
                marginTop: -10,
              }}
              onPress={() => navigation.goBack()}
            />
          </View>
        }
        centerComponent={
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: 20,
                fontFamily: "Roboto-bold",
              }}
            >
              Chi tiết dịch vụ
            </Text>
          </View>
        }
      />
      <ScrollView showsVerticalScrollIndicator={false} horizontal={false}>
        {/* UI cho tiết dịch vụ */}
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-start",
            width: "100%",
            gap: 20,
            marginTop: 20,
            height: "auto",

            backgroundColor: "#FCF8F3",
            flexDirection: "column",
            borderRadius: 12,
          }}
        >
          <Image
            source={{
              uri: `${localHostEnv}/${thumbnail.replace(/\\/g, "/")}`,
            }}
            style={{
              width: "100%",
              height: 300,
              objectFit: "cover",
              borderTopLeftRadius: 12,
              borderTopRightRadius: 12,
            }}
          />
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",

              marginRight: 10,
            }}
          >
            <Text
              style={{
                color: "#496989",
                fontFamily: "Roboto-BoldItalic",
                fontSize: 30,
              }}
            >
              {"  "}
              {servicesName}
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              marginLeft: 10,
              marginRight: 10,
              height: "auto",
              width: "100%",
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                flexDirection: "row",
                width: "100%",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 20,
                }}
              >
                Thông tin chi tiết
              </Text>
              <Icon name="book" size={20} />
            </View>
            <Text
              style={{
                multiLine: true,
                width: 350,

                color: "#496989",
                fontFamily: "Roboto",
                fontSize: 18,
              }}
            >
              {description}
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              marginLeft: 10,
              marginRight: 10,
              height: "auto",
              width: "100%",
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                flexDirection: "row",
                width: "100%",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 20,
                }}
              >
                kinh nghiệm làm việc
              </Text>
            </View>
            <Text
              style={{
                multiLine: true,
                width: 350,

                color: "#496989",
                fontFamily: "Roboto",
                fontSize: 18,
              }}
            >
              {exp}
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              marginLeft: 10,
              marginRight: 10,
              height: "auto",
              width: "100%",
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                flexDirection: "row",
                width: "100%",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 20,
                }}
              >
                Địa chỉ dịch vụ
              </Text>
              <Icon name="location-pin" size={20} color="#FF4C4C" />
            </View>
            <Text
              style={{
                multiLine: true,
                width: 350,

                color: "#496989",
                fontFamily: "Roboto",
                fontSize: 18,
              }}
            >
              {area}
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              marginLeft: 10,
              marginRight: 10,
              height: "auto",
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <Icon name="credit" type="entypo" size={40} color="#45474B" />
            </View>
            <Text
              style={{
                color: "#E4003A",
                fontFamily: "Roboto-bold",
                fontSize: 40,
              }}
            >
              : {price}
            </Text>
            <Text
              style={{
                color: "#496989",
                fontFamily: "Roboto",
                fontSize: 20,
              }}
            >
              /VNĐ
            </Text>
          </View>
          <Button
            title="Đặt dịch vụ"
            buttonStyle={{
              backgroundColor: "#57A6A1",
              width: 350,
              maxWidth: "100%",
              marginLeft: 20,
              marginRight: 20,
              height: 50,
              marginBottom: 20,
              borderRadius: 8,
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
            }}
            titleStyle={{
              fontSize: 20,
              fontWeight: "bold",
            }}
            onPress={() =>
              navigation.navigate("BookingScreen", {
                serviceId: servicesId,
                serviceName: servicesName,
                servicePrice: price,
                serviceImage: thumbnail,
                userId: userInfo._id,
              })
            }
          />
        </View>
        {/* UI đánh giá khách hàng */}
        <View
          style={{
            height: "auto",

            backgroundColor: "#FCF8F3",
            marginTop: 20,
            borderRadius: 12,
            padding: 10,
          }}
        >
          <Text
            style={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 20,
              marginBottom: 10,
            }}
          >
            Đánh giá dịch vụ:
          </Text>
          <Comment userId={userInfo._id} servicesId={servicesId} />
        </View>
      </ScrollView>
    </View>
  );
};

export default DetailServicesScreen;
