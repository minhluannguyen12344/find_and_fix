// @ts-nocheck
import { Text, View } from "react-native";
//Tabs Screens
import UserNotificationScreen from "./UserNotificationScreen";
import UserOrderScreen from "./UserOrderScreen";
import UserInfoScreen from "./UserInfoScreen";

import HomeScreen from "./HomeScreen";
//Components library
import { Icon } from "@rneui/themed";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const Tab = createBottomTabNavigator();
const UserMainLayout = ({ route, navigation }) => {
  const { userId } = route.params;
  return (
    <Tab.Navigator
      headerShow={false}
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarStyle: { backgroundColor: "#57A6A1", height: 60, gap: 10 },
        tabBarActiveTintColor: "#005C78",
        tabBarInactiveTintColor: "white",

        tabBarIcon: ({ color, size }) => {
          let iconName;
          if (route.name === "Home") {
            iconName = "home";
          } else if (route.name === "Order") {
            iconName = "list-ul";
          } else if (route.name === "Notification") {
            iconName = "bell";
          } else if (route.name === "Info") {
            iconName = "user";
          }

          return (
            <Icon
              name={iconName}
              size={size}
              color={color}
              type="font-awesome-5"
            />
          );
        },
        tabBarLabel: ({ focused, color, size }) => {
          let labelName;
          if (route.name === "Home") {
            labelName = "Trang chủ";
          } else if (route.name === "Order") {
            labelName = "Hoạt động";
          } else if (route.name === "Notification") {
            labelName = "Thông báo";
          } else if (route.name === "Info") {
            labelName = "Thông tin";
          }
          return (
            <Text style={{ color: color, fontSize: size }}>{labelName}</Text>
          );
        },
      })}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        initialParams={{ userId: userId }}
      />
      <Tab.Screen
        name="Order"
        component={UserOrderScreen}
        initialParams={{ userId: userId }}
      />
      <Tab.Screen
        name="Notification"
        component={UserNotificationScreen}
        initialParams={{ userId: userId }}
      />
      <Tab.Screen
        name="Info"
        component={UserInfoScreen}
        initialParams={{ userId: userId }}
      />
    </Tab.Navigator>
  );
};

export default UserMainLayout;
