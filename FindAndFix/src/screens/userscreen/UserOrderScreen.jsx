import { Text, View, ScrollView, Image } from "react-native";
import AppBar from "../../components/AppBar";
import { Tab, Button, Icon } from "@rneui/themed";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";
import localHostEnv from "../../config/localhost_env";
import axios from "axios";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
const UserOrderScreen = () => {
  const [orderData, setOrderData] = useState([]);
  const userInfo = useSelector((state) => state.user.userInfo);

  const [reload, setReload] = useState(false);
  useEffect(() => {
    const getOrderData = async () => {
      try {
        if (!userInfo._id || !localHostEnv) {
          console.error("userInfo._id or localHostEnv is undefined");
          return;
        }
        const url = `${localHostEnv}/order/user/${userInfo._id}`;
        console.log("Fetching order data from URL:", url);
        const response = await axios.get(url);
        setOrderData(response.data);
        console.log("Order data fetched successfully:", response.data);
      } catch (e) {
        console.error("Failed to fetch order data:", e);
      }
    };
    getOrderData();
  }, [userInfo._id, reload]);
  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <AppBar />

      <ScrollView
        style={{
          width: "100%",
        }}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          gap: 10,
        }}
      >
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#FCF8F3",
            borderRadius: 12,

            width: "95%",
            height: 60,
            padding: 10,
            marginTop: 20,
          }}
        >
          <Text
            style={{
              fontSize: 20,
              fontWeight: "bold",
              color: "#45474B",
            }}
          >
            Đơn hàng đang có
          </Text>
        </View>
        <Button
          title="Làm mới danh sách"
          onPress={() => setReload((prev) => !prev)}
          buttonStyle={{
            backgroundColor: "#F0EBE3",

            width: "auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: 40,
            gap: 10,
            borderRadius: 100,
            borderColor: "grey",
            marginTop: 10,
            borderWidth: 1,
          }}
        >
          <Icon name="reload" size={20} type="ionicon" color="#45474B" />
        </Button>
        <View
          style={{
            width: "100%",
            marginBottom: 100,
            padding: 10,
            display: "flex",
            flexDirection: "column",
            gap: 10,
          }}
        >
          {orderData.map((order, key) => (
            <View
              key={key}
              style={{
                width: "100%",
                marginTop: 20,
                borderRadius: 12,
                backgroundColor: "#FCF8F3",
                gap: 20,

                paddingBottom: 20,
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "auto",
                  backgroundColor: "#F9E2AF",
                  borderTopLeftRadius: 12,
                  borderTopEndRadius: 12,
                  padding: 10,
                }}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "flex-start",
                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      color: "#45474B",

                      fontSize: 16,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    Mã đơn:
                  </Text>
                  <Text
                    style={{
                      color: "#496989",
                      fontWeight: "bold",
                      fontSize: 16,
                    }}
                  >
                    {order._id}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Ngày xử Lý:
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontFamily: "Roboto-Bold",
                    fontSize: 16,
                  }}
                >
                  {new Date(order.bookingDate).toLocaleString("vi-VN")}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Ngày đặt:{" "}
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontFamily: "Roboto-Bold",
                    fontSize: 16,
                  }}
                >
                  {new Date(order.createdAt).toLocaleString("vi-VN")}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Số điện thoại:
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  {order.phone}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-end",
                  paddingLeft: 10,
                  paddingRight: 10,
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    color: "#45474B",

                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Tên người đặt
                </Text>
                <Text
                  style={{
                    color: "#496989",

                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  {order.name}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Trạng thái:{" "}
                </Text>
                <Text
                  style={{
                    color: "grey",
                    fontSize: 16,
                  }}
                >
                  {order.status === "pending" ? (
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        backgroundColor: "#ECB159",
                        alignItems: "center",
                        padding: 5,
                        borderRadius: 100,
                        gap: 5,
                      }}
                    >
                      <MaterialIcons name="schedule" size={20} color="white" />
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "Roboto-Bold",
                        }}
                      >
                        Đang chờ xử lý
                      </Text>
                    </View>
                  ) : order.status === "onDoing" ? (
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "#478CCF",
                        borderRadius: 100,
                        padding: 5,
                        gap: 5,
                      }}
                    >
                      <MaterialIcons name="output" size={20} color="white" />
                      <Text
                        style={{
                          fontFamily: "Roboto-Bold",
                          color: "white",
                        }}
                      >
                        Đang tiến hành
                      </Text>
                    </View>
                  ) : order.status === "Complete" ? (
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "#88D66C",
                        borderRadius: 100,
                        gap: 5,
                        padding: 5,
                      }}
                    >
                      <MaterialIcons name="check" size={20} color="white" />
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "Roboto-Bold",
                        }}
                      >
                        Hoàn Thành
                      </Text>
                    </View>
                  ) : order.status === "Cancel" ? (
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "#FF4C4C",
                        borderRadius: 100,
                        padding: 5,
                        gap: 5,
                      }}
                    >
                      <MaterialIcons name="delete" size={20} color="white" />
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "Roboto-Bold",
                        }}
                      >
                        Hủy đơn
                      </Text>
                    </View>
                  ) : null}
                </Text>
              </View>
              <View
                style={{
                  width: "100%",
                  height: 1,
                  backgroundColor: "grey",
                }}
              ></View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  gap: 5,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",

                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Loại dịch vụ đặt:{" "}
                </Text>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginLeft: "auto",
                    marginRight: "auto",
                    alignItems: "center",
                    gap: 10,
                    backgroundColor: "#EADBC8",
                    width: "95%",
                    borderRadius: 12,
                  }}
                >
                  <Image
                    style={{
                      width: 100,
                      height: 100,
                      borderTopLeftRadius: 12,
                      borderBottomLeftRadius: 12,
                    }}
                    source={{
                      uri: `${localHostEnv}/${order.services[0]?.thumbnail.replace(
                        /\\/g,
                        "/"
                      )}`,
                    }}
                  ></Image>
                  <Text
                    style={{
                      color: "#496989",
                      fontSize: 20,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    {order.services[0]?.servicesName}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  justifyContent: "center",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Địa chỉ xử lý
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontFamily: "Roboto-Bold",
                    fontSize: 16,
                  }}
                >
                  {order.address}
                </Text>
              </View>

              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Ghi chú:
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  {order.note}
                </Text>
              </View>
              <View
                style={{
                  width: "100%",
                  backgroundColor: "grey",
                  height: 1,
                }}
              ></View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "flex-end",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",

                    fontSize: 25,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Tổng tiền:{" "}
                </Text>
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "row",
                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      color: "#FF4C4C",
                      fontSize: 25,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    {order.price}
                  </Text>
                  <Text
                    style={{
                      color: "#496989",
                      textDecorationLine: "underline",
                      fontSize: 16,
                      fontFamily: "Roboto-BoldItalic",
                    }}
                  >
                    đ
                  </Text>
                </View>
              </View>
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default UserOrderScreen;
