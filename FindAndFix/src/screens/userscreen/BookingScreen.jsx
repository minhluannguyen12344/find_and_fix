import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Image,
} from "react-native";

import React, { useEffect, useState } from "react";
import { Header, Icon, Button } from "@rneui/themed";
import DateTimePicker from "@react-native-community/datetimepicker";
import Toast from "react-native-toast-message";
import axios from "axios";
import localHostEnv from "../../config/localhost_env";
import { useFonts } from "expo-font";
import { useDispatch, useSelector } from "react-redux";
import { Linking } from "react-native";
const BookingScreen = ({ route, navigation }) => {
  const { serviceId, serviceName, serviceImage, servicePrice, userId } =
    route.params;
  const userInfo = useSelector((state) => state.user.userInfo);
  const imageUrl = `${localHostEnv}/${serviceImage.replace(/\\/g, "/")}`;
  const [fontsLoaded] = useFonts({
    Roboto: require("../../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const [address, setAddress] = useState("");
  const [name, setName] = useState("");
  const [phonenumber, setPhonenumber] = useState("");

  const [conditions, setConditions] = useState("");
  const [errors, setErrors] = useState({});
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [receivedUrl, setReceivedUrl] = useState(null);
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false); // Hide the picker after selection
    setDate(currentDate);
  };
  const showDateTimePicker = () => {
    setShow(true);
  };

  const handleAddressChange = (text) => {
    setAddress(text);
  };
  const handleNameChange = (text) => {
    setName(text);
  };
  const handlePhonenumberChange = (text) => {
    setPhonenumber(text);
  };
  const handleConditionsChange = (text) => {
    setConditions(text);
  };

  // validation booking
  const validationBooking = () => {
    let newErrors = {};
    if (phonenumber === "") {
      newErrors.phonenumber = "Vui lòng nhập số điện thoại của bạn";
    }
    if (address === "") {
      newErrors.address = "Vui lòng nhập địa chỉ của bạn";
    }
    if (name === "") {
      newErrors.name = "Vui lòng nhập tên của bạn";
    }

    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };
  // handle submit booking
  const handleSubmitBooking = async () => {
    const isValid = validationBooking();
    if (isValid) {
      try {
        const paymentResponse = await axios.post(
          `${localHostEnv}/momo/payment`,
          {
            amount: servicePrice,
          }
        );
        console.log(paymentResponse.data.payUrl);
        const payUrl = paymentResponse.data.payUrl;
        Linking.openURL(payUrl);
        console.log("Payment response:");
      } catch (error) {
        console.error("Error processing payment:", error);
      }
    }
  };
  useEffect(() => {
    const handleOpenURL = async (event) => {
      console.log("Incoming URL:", event.url);
      setReceivedUrl(event.url); // Update state with the received URL
    };

    const subscription = Linking.addEventListener("url", handleOpenURL);

    // Return a cleanup function that removes the event listener
    return () => {
      subscription.remove();
    };
  }, []);

  useEffect(() => {
    // Ensure receivedUrl is not null or empty
    if (receivedUrl) {
      const url = new URL(receivedUrl);
      const params = new URLSearchParams(url.search);
      const resultCode = params.get("resultCode");
      if (resultCode === "0") {
        try {
          const response = axios.post(`${localHostEnv}/order/`, {
            name: name,
            address: address,
            phone: phonenumber,
            bookingDate: date,
            note: conditions,
            services: serviceId,
            price: servicePrice,
            createdBy: userId,
            userEmail: userInfo.email,
          });

          setTimeout(() => {
            navigation.navigate("Order");
          }, 2000);
        } catch (error) {
          console.error("Error creating order:", error);
        }
      } else {
        console.error("Bạn đã hủy đơn hàng:");
      }
    }
  }, [receivedUrl]);
  // handle submit pay
  const handleSubmitpay = async () => {
    // try {
    //   const paymentMethod = await axios.post(`${localHostEnv}/momo/payment`, {
    //     amount: servicePrice,
    //   });
    //   console.log(paymentMethod.data.payUrl);
    //   const payUrl = paymentMethod.data.payUrl;
    //   setLink(payUrl);
    //   console.log(payUrl);
    //   Linking.openURL(payUrl);
    // } catch (error) {
    //   console.log(error);
    // }
  };
  return (
    <View
      style={{
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        flexDirection: "column",
        backgroundColor: "white",
      }}
    >
      <Header
        backgroundColor="#57A6A1"
        containerStyle={{
          height: 100,
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: 5,
        }}
        leftComponent={
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Icon
              type="font-awnsome-5"
              name="chevron-left"
              color="white"
              size={50}
              containerStyle={{
                marginTop: -10,
              }}
              onPress={() => navigation.goBack()}
            />
          </View>
        }
        centerComponent={
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: 20,
                fontWeight: "bold",
              }}
            >
              Đặt dịch vụ
            </Text>
          </View>
        }
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        horizontal={false}
        style={{
          width: "100%",
        }}
      >
        <View
          style={{
            width: "100%",
            height: 4,
            backgroundColor: "#EEEDEB",
          }}
        ></View>
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-start",
            flexDirection: "column",
            width: "100%",
            height: "auto",
            padding: 10,
            marginTop: 10,
          }}
        >
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 10,
            }}
          >
            <Icon
              name="map-marker-alt"
              size={15}
              type="font-awesome-5"
              color="red"
            />
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 20,
              }}
            >
              Địa chỉ
            </Text>
          </View>
          <TextInput
            style={{
              width: "100%",
              marginTop: 10,
              padding: 10,
              fontSize: 18,
              borderRadius: 8,
              height: "auto",
              backgroundColor: "#F0EBE3",
            }}
            placeholder="Nhập địa chỉ của bạn..."
            onChangeText={handleAddressChange}
            multiline={true}
            numberOfLines={2}
            value={address}
          />
          {errors.address && (
            <Text
              style={{
                color: "red",
                fontSize: 16,
              }}
            >
              {errors.address}
            </Text>
          )}
        </View>
        <View
          style={{
            width: "100%",
            height: 4,
            backgroundColor: "#EEEDEB",
          }}
        ></View>
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-start",
            flexDirection: "column",
            width: "100%",
            height: "auto",
            padding: 10,
            marginTop: 10,
          }}
        >
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 10,
            }}
          >
            <Icon name="user" size={15} type="font-awesome-5" color="grey" />
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 20,
              }}
            >
              Tên người đặt
            </Text>
          </View>
          <TextInput
            style={{
              width: "100%",
              marginTop: 10,
              padding: 10,
              borderRadius: 8,
              backgroundColor: "#F0EBE3",
              fontSize: 18,
            }}
            placeholder="Nhập tên của bạn..."
            onChangeText={handleNameChange}
            multiline={true}
            numberOfLines={2}
            value={name}
          />
          {errors.name && (
            <Text
              style={{
                color: "red",
                fontSize: 16,
              }}
            >
              {errors.name}
            </Text>
          )}
        </View>
        <View
          style={{
            width: "100%",
            height: 4,
            backgroundColor: "#EEEDEB",
          }}
        ></View>
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-start",
            flexDirection: "column",
            width: "100%",
            height: "auto",
            padding: 10,
            marginTop: 10,
          }}
        >
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 10,
            }}
          >
            <Icon name="phone" size={15} type="font-awesome-5" color="grey" />
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 20,
              }}
            >
              Số điện thoại
            </Text>
          </View>
          <TextInput
            style={{
              width: "100%",
              marginTop: 10,
              padding: 10,
              backgroundColor: "#F0EBE3",
              borderRadius: 8,
              fontSize: 18,
            }}
            placeholder="Nhập số điện thoại liên lạc..."
            onChangeText={handlePhonenumberChange}
            multiline={true}
            numberOfLines={1}
            value={phonenumber}
            keyboardType="numeric"
          />
          {errors.phonenumber && (
            <Text
              style={{
                color: "red",
                fontSize: 16,
              }}
            >
              {errors.phonenumber}
            </Text>
          )}
        </View>
        <View
          style={{
            width: "100%",
            height: 4,
            backgroundColor: "#EEEDEB",
          }}
        ></View>
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-start",
            flexDirection: "column",
            gap: 10,
            padding: 10,
          }}
        >
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 10,
            }}
          >
            <Icon name="clock" size={15} type="font-awesome-5" color="grey" />
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 20,
              }}
            >
              Chọn thời gian xử lý
            </Text>
          </View>

          <Button
            onPress={showDateTimePicker}
            title="Chọn thời gian xử lý"
            buttonStyle={{
              backgroundColor: "#57A6A1",
              width: "auto",
              height: 40,
              borderRadius: 8,
            }}
          />
          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              value={date}
              mode="date"
              is24Hour={true}
              display="default"
              onChange={onChange}
            />
          )}
          <Text
            style={{
              color: "#45474B",
              fontFamily: "Roboto-Medium",
            }}
          >
            Lịch hẹn:{" "}
            {new Intl.DateTimeFormat("vi-VN", {
              day: "numeric",
              month: "long",
              year: "numeric",
            }).format(date)}
          </Text>
        </View>
        <View
          style={{
            width: "100%",
            height: 4,
            backgroundColor: "#EEEDEB",
          }}
        ></View>
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flext-start",
            flexDirection: "column",
            width: "100%",
            height: "auto",
            padding: 10,
            marginTop: 10,
          }}
        >
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 10,
            }}
          >
            <Icon name="pen" size={15} type="font-awesome-5" color="grey" />
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 20,
              }}
            >
              Yêu cầu thêm
            </Text>
          </View>
          <TextInput
            style={{
              width: "100%",
              borderRadius: 8,
              backgroundColor: "#F0EBE3",
              marginTop: 10,
              padding: 10,
              fontSize: 18,
            }}
            placeholder="Nhập yêu cầu thêm hoặc tình trạng hư hỏng(không bắt buột ghi)..."
            onChangeText={handleConditionsChange}
            multiline={true}
            numberOfLines={5}
            value={conditions}
          />
        </View>
        <View
          style={{
            width: "100%",
            height: 4,
            backgroundColor: "#EEEDEB",
          }}
        ></View>
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flext-start",
            flexDirection: "column",
            width: "100%",
            height: "auto",
            padding: 10,
            marginTop: 10,
          }}
        >
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 10,
            }}
          >
            <Icon name="wrench" size={15} type="font-awesome-5" color="grey" />
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 20,
              }}
            >
              Dịch vụ đã đặt
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "start",
              alignItems: "flex-start",
              flexDirection: "row",
              gap: 10,
              width: "100%",
              height: "auto",
              borderRadius: 8,
              backgroundColor: "#F0EBE3",
              // borderWidth: 1,
              padding: 10,
              borderColor: "#7AB2B2",
              marginTop: 10,
            }}
          >
            <Image
              source={{ uri: imageUrl }}
              style={{
                width: 100,
                height: 100,
                borderRadius: 12,
              }}
            />
            <Text
              style={{
                color: "#4D869C",
                fontWeight: "bold",
                fontSize: 16,
              }}
            >
              {serviceName}
            </Text>
          </View>
        </View>
        <View
          style={{
            width: "100%",
            height: 4,
            backgroundColor: "#EEEDEB",
          }}
        ></View>
        <View
          style={{
            display: "flex",
            justifyContent: "start",
            alignItems: "flex-start",
            flexDirection: "column",
            gap: 10,
            width: "100%",
            height: "auto",
            padding: 10,
            marginBottom: 200,
          }}
        >
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-end",
              flexDirection: "row",
              gap: 10,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 30,
              }}
            >
              Tổng tiền :
            </Text>
            <Text
              style={{
                color: "#E4003A",
                fontFamily: "Roboto-Bold",
                fontSize: 25,
              }}
            >
              {servicePrice} VNĐ
            </Text>
          </View>

          <Button
            title="Thanh Toán"
            titleStyle={{ color: "white", fontFamily: "Roboto-Bold" }}
            onPress={handleSubmitBooking}
            buttonStyle={{
              width: 390,
              maxWidth: "100%",
              marginTop: 10,
              height: 50,
              borderRadius: 12,
              borderColor: "#57A6A1",
            }}
          />
          {/* <Button
            title="test"
            titleStyle={{ color: "white", fontFamily: "Roboto-Bold" }}
            onPress={handleSubmitpay}
            buttonStyle={{
              width: 390,
              maxWidth: "100%",
              marginTop: 10,
              height: 50,
              borderRadius: 12,
              borderColor: "#57A6A1",
            }}
          /> */}
        </View>
      </ScrollView>
    </View>
  );
};

export default BookingScreen;
