import { Text, View, ScrollView, Image, TextInput } from "react-native";
import AppBar from "../../components/AppBar";
import { Button, Icon, Dialog } from "@rneui/themed";
import { useDispatch, useSelector } from "react-redux";
import { updateUserInfo } from "../../config/reduxStore";
import axios from "axios";
import Toast from "react-native-toast-message";
import * as ImagePicker from "expo-image-picker";
import localHostEnv from "../../config/localhost_env";
import { useState, useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import { useFonts } from "expo-font";
import AsyncStorage from "@react-native-async-storage/async-storage";

const UserInfoScreen = () => {
  const navigation = useNavigation();
  const [fontsLoaded] = useFonts({
    Roboto: require("../../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const showToast = () => {
    Toast.show({
      type: "success",
      visibilityTime: 800,
      text1: "Cập nhật  thành công",
    });
  };
  const errorToast = () => {
    Toast.show({
      type: "error",
      visibilityTime: 800,
      text1: "Cập nhật thất bại",
    });
  };
  const userInfo = useSelector((state) => state.user.userInfo);
  const imageUrl = `${localHostEnv}/${userInfo?.avatar.replace(/\\/g, "/")}`;
  const dispatch = useDispatch();
  const [name, setName] = useState(userInfo?.fullname);
  const [phone, setPhone] = useState(userInfo?.phonenumber);
  const [address, setAddress] = useState(userInfo?.address);
  const [image, setImage] = useState(imageUrl);
  const [errors, setErrors] = useState({});
  const [isVisible, setIsVisible] = useState(false);
  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.assets[0].uri);
    }
  };
  const validationUpdate = () => {
    let newErrors = {};
    if (name === "") {
      newErrors.name = "Vui điền tên của bạn";
    }
    if (phone === "") {
      newErrors.phone = "Vui lòng điền số điện thoại của bạn";
    }
    if (address === "") {
      newErrors.address = "Vui lòng điền địa chỉ của bạn";
    }
    if (image === "") {
      newErrors.image = "Vui lòng chọn ảnh đại diện";
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };
  const handleUpdate = async () => {
    if (validationUpdate()) {
      console.log(
        "name: " + name,
        "phone: " + phone,
        "address: " + address,
        "image: " + image
      );

      const formUpdateData = new FormData();
      formUpdateData.append("fullname", name);
      formUpdateData.append("phonenumber", phone);
      formUpdateData.append("address", address);
      formUpdateData.append("image", {
        uri: image,
        type: "image",
        name: image.split("/").pop(),
      });
      console.log(formUpdateData);
      try {
        const axiosResponse = await axios.put(
          `${localHostEnv}/user/${userInfo._id}`,
          formUpdateData,
          {
            headers: {
              Accept: "application/json",
            },
          }
        );
        showToast();
        dispatch(updateUserInfo(axiosResponse.data));
        // setName(userInfo.fullname);
        // setAddress(userInfo.address);
        // setPhone(userInfo.phonenumber);
        // setImage(imageUrl); // Your update logic here
        console.log(axiosResponse);
      } catch (error) {
        showErrorToast();
        if (error.response) {
          errorToast();
          console.error(error.response.data.content);
        }
      }
    }
    setIsVisible(false);

    // Close the dialog after updating
  };
  const handleLogout = async () => {
    try {
      await AsyncStorage.clear();
      console.log("Storage successfully cleared!");
      navigation.navigate("LoginScreen");
    } catch (e) {
      console.error("Failed to clear the async storage.", e);
    }
  };
  useEffect(() => {
    setName(userInfo.fullname);
    setAddress(userInfo.address);
    setPhone(userInfo.phonenumber);
    setImage(imageUrl);
  }, [userInfo]);
  return (
    <View
      style={{
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        width: "100%",
        height: "100vh",
        backgroundColor: "#EEEDEB",
        flexDirection: "column",
      }}
    >
      <AppBar />
      <ScrollView
        style={{
          width: "100%",
          marginBottom: 100,
        }}
        contentContainerStyle={{
          display: "flex",
          justifyContent: "start",
          alignItems: "center",
          flexDirection: "column",
          gap: 10,
        }}
      >
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "95%",
            borderRadius: 10,
            backgroundColor: "#FCF8F3",
            height: "auto",
            padding: 20,
            gap: 10,
            marginTop: 10,
          }}
        >
          <Text
            style={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 30,
            }}
          >
            Thông tin cá nhân
          </Text>
          <Image
            source={{ uri: imageUrl }}
            style={{
              width: 150,
              height: 150,
              borderRadius: 100,
              borderColor: "#45474B",
              borderWidth: 3,
            }}
          />
          {/* <Button
            title="Cập nhật thông tin"
            buttonStyle={{
              backgroundColor: "#57A6A1",
              width: 350,
              height: 50,
              borderRadius: 12,
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
            }}
            titleStyle={{
              fontSize: 20,
              fontWeight: "bold",
            }}
            
          /> */}
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              height: 50,
              gap: 5,
            }}
          >
            <Text
              onPress={() => setIsVisible(true)}
              style={{
                color: "#496989",
                fontFamily: "Roboto-BoldItalic",
                textDecorationLine: "underline",
                fontSize: 20,
              }}
            >
              Cập nhật thông tin
            </Text>
            <Icon type="font-awesome-5" name="edit" size={20} color="#4D4C7D" />
          </View>
        </View>

        <View
          style={{
            display: "flex",
            justifyContent: "center",
            width: "95%",
            backgroundColor: "#FCF8F3",
            borderRadius: 10,
            alignItems: "flex-start",
            flexDirection: "column",
            padding: 5,
          }}
        >
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              width: "100%",
              padding: 10,
              gap: 5,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 16,
              }}
            >
              Tên người dùng:
            </Text>
            <View
              style={{
                backgroundColor: "#F0EBE3",
                borderRadius: 10,
                display: "flex",
                padding: 10,
                justifyContent: "center",
                width: "100%",
                height: 50,
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                }}
              >
                {userInfo?.fullname}
              </Text>
            </View>
          </View>

          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              width: "100%",
              padding: 10,
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                gap: 5,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 16,
                }}
              >
                Email
              </Text>
            </View>

            <View
              style={{
                backgroundColor: "#F0EBE3",
                borderRadius: 10,
                display: "flex",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                width: "100%",
                height: 50,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                }}
              >
                {userInfo?.email}
              </Text>
              <Icon
                type="font-awesome-5"
                name="envelope"
                size={20}
                color="#4D4C7D"
              />
            </View>
          </View>

          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              width: "100%",
              padding: 10,
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                gap: 5,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 16,
                }}
              >
                Địa chỉ
              </Text>
            </View>

            <View
              style={{
                backgroundColor: "#F0EBE3",
                borderRadius: 10,
                display: "flex",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                width: "100%",
                height: 50,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                }}
              >
                {userInfo?.address}
              </Text>
              <Icon
                type="font-awesome-5"
                name="map-marker-alt"
                size={20}
                color="#4D4C7D"
              />
            </View>
          </View>

          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              width: "100%",
              padding: 10,
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                gap: 5,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 16,
                }}
              >
                Số điện thoại
              </Text>
            </View>

            <View
              style={{
                backgroundColor: "#F0EBE3",
                borderRadius: 10,
                display: "flex",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                width: "100%",
                height: 50,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                }}
              >
                {userInfo?.phonenumber}
              </Text>
              <Icon
                type="font-awesome-5"
                name="phone"
                size={20}
                color="#4D4C7D"
              />
            </View>
          </View>

          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              width: "100%",
              padding: 10,
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                gap: 5,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 16,
                }}
              >
                Loại tài khoản
              </Text>
            </View>

            <View
              style={{
                backgroundColor: "#F0EBE3",
                borderRadius: 10,
                display: "flex",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                width: "100%",
                height: 50,
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                }}
              >
                {userInfo?.role}
              </Text>
              <Icon
                type="font-awesome-5"
                name="user-tag"
                size={20}
                color="#4D4C7D"
              />
            </View>
          </View>
          <View
            style={{
              width: "100%",
              padding: 10,
            }}
          >
            <Button
              // title="Đăng xuất"
              buttonStyle={{
                backgroundColor: "white",
                width: "100%",
                height: 50,
                borderRadius: 12,
                borderColor: "#EE4E4E",
                borderWidth: 1,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                gap: 10,
                flexDirection: "row",
              }}
              titleStyle={{
                fontSize: 20,
                fontFamily: "Roboto-Bold",
                color: "#EE4E4E",
              }}
              // onPress={() => {navigation.navigate("LoginScreen")}}
              onPress={handleLogout}
            >
              <Text
                style={{
                  fontSize: 20,
                  fontFamily: "Roboto-Bold",
                  color: "#EE4E4E",
                }}
              >
                Đăng xuất
              </Text>
              <Icon name="log-out" size={20} type="entypo" color="#EE4E4E" />
            </Button>
          </View>
        </View>

        <Dialog
          isVisible={isVisible}
          onBackdropPress={() => setIsVisible(false)}
          overlayStyle={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            direction: "column",
            padding: 10,
            gap: 10,
          }}
        >
          <Dialog.Title
            title="Cập nhật thông tin"
            titleStyle={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 30,
            }}
          />
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              width: "100%",
              gap: 10,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontSize: 20,
                fontFamily: "Roboto-Bold",
              }}
            >
              Số điện thoại
            </Text>
            <View
              style={{
                height: 50,
                borderRadius: 12,
                width: "100%",
                padding: 10,
                borderColor: "#F0EBE3",
                borderWidth: 1,
                display: "flex",
                gap: 20,
                justifyContent: "center",
                alignItems: "flex-start",
              }}
            >
              <TextInput
                style={{
                  fontSize: 18,
                  fontFamily: "Roboto",
                  color: "#496989",
                }}
                onChangeText={(text) => setPhone(text)}
                numberOfLines={1}
                value={phone}
              />
            </View>
            {errors.phone && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {errors.phone}
              </Text>
            )}
          </View>

          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              width: "100%",
              gap: 10,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontSize: 20,
                fontFamily: "Roboto-Bold",
              }}
            >
              Địa chỉ
            </Text>
            <View
              style={{
                height: 50,
                borderRadius: 12,

                width: "100%",
                padding: 10,
                borderColor: "#F0EBE3",
                borderWidth: 1,
                display: "flex",
                gap: 20,
                justifyContent: "center",
                alignItems: "flex-start",
              }}
            >
              <TextInput
                style={{
                  fontSize: 18,
                  fontFamily: "Roboto",
                  color: "#496989",
                }}
                onChangeText={(text) => setAddress(text)}
                numberOfLines={1}
                value={address}
              />
            </View>
            {errors.address && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {errors.address}
              </Text>
            )}
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              width: "100%",
              gap: 10,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontSize: 20,
                fontFamily: "Roboto-Bold",
              }}
            >
              Tên
            </Text>
            <View
              style={{
                height: 50,
                borderRadius: 12,

                width: "100%",
                padding: 10,
                borderColor: "#F0EBE3",
                borderWidth: 1,
                display: "flex",
                gap: 20,
                justifyContent: "center",
                alignItems: "flex-start",
              }}
            >
              <TextInput
                style={{
                  fontSize: 18,
                  fontFamily: "Roboto",
                  color: "#496989",
                }}
                onChangeText={(text) => setName(text)}
                numberOfLines={1}
                value={name}
              />
            </View>
            {errors.name && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {errors.name}
              </Text>
            )}
          </View>
          <View
            style={{
              width: "100%",
              gap: 10,
            }}
          >
            <Button
              buttonStyle={{
                backgroundColor: "white",
                width: "100%",
                borderColor: "#F0EBE3",

                borderWidth: 1,
                height: 50,
                gap: 10,
                borderRadius: 12,
              }}
              onPress={pickImage}
              titleStyle={{
                fontSize: 20,
                fontWeight: "bold",
              }}
            >
              <Text
                style={{
                  fontSize: 20,
                  fontFamily: "Roboto-Bold",
                  color: "#A7C4BC",
                }}
              >
                {" "}
                Cập nhật ảnh
              </Text>
              <Icon name="upload" size={20} color="#A7C4BC" />
            </Button>
            {image && (
              <Image
                source={{ uri: image }}
                style={{
                  width: 150,
                  height: 150,
                  borderRadius: 100,
                  borderColor: "#45474B",
                  borderWidth: 2,
                  transform: [{ translateX: 125 }],
                }}
              />
            )}
          </View>
          <View
            style={{
              width: "100%",
            }}
          >
            <Button
              title="Cập nhật"
              onPress={() => handleUpdate()}
              buttonStyle={{
                backgroundColor: "#57A6A1",
                width: "100%",
                height: 50,
                borderRadius: 12,
              }}
              titleStyle={{
                fontSize: 20,
                fontWeight: "bold",
              }}
            />

            <Button
              title="quay về"
              buttonStyle={{
                backgroundColor: "white",
                width: "100%",
                height: 50,
                marginTop: 10,
                borderRadius: 12,
                borderColor: "#4D869C",
                borderWidth: 1,
              }}
              onPress={() => setIsVisible(false)}
              titleStyle={{
                fontSize: 20,
                fontWeight: "bold",
                color: "#4D869C",
              }}
            />
          </View>
        </Dialog>
      </ScrollView>
    </View>
  );
};

export default UserInfoScreen;
