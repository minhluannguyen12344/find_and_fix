import React, { useState } from "react";

import axios from "axios";
import {
  Text,
  View,
  Image,
  TextInput,
  Keyboard,
  ScrollView,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { Header, Button, Input, Icon } from "@rneui/themed";

import { useForm, Controller } from "react-hook-form";

import { useFonts } from "expo-font";

const SignUpScreen = ({ route, navigation }) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  //get role data from ChooseRoleScreen
  const { role } = route.params;
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({});
  const onSubmit = async (data) => {
    const mergeInfo = { ...data, role: role };
    const formData = new FormData();
    formData.append("email", mergeInfo.Email);
    formData.append("password", mergeInfo.password);
    formData.append("fullname", mergeInfo.Username);
    formData.append("role", mergeInfo.role);
    formData.append("address", mergeInfo.address);
    formData.append("phonenumber", mergeInfo.phonenumber);
    formData.append("image", {
      uri: mergeInfo.image,
      type: "image",
      name: mergeInfo.image.split("/").pop(),
    });

    navigation.navigate("OtpVerifyScreen", {
      email: mergeInfo.Email,
      password: mergeInfo.password,
      fullname: mergeInfo.Username,
      role: mergeInfo.role,
      address: mergeInfo.address,
      phonenumber: mergeInfo.phonenumber,
      avatar: mergeInfo.image,
    });
  };
  return (
    //Header
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        backgroundColor: "white",
      }}
      onTouchStart={() => Keyboard.dismiss()}
    >
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          marginVertical: 20,
          marginBottom: 200,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          width: "100%",
          maxWidth: 392,
          marginTop: 30,
        }}
      >
        {/*Icon App View*/}
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 30,
          }}
        >
          <Text
            style={{
              fontSize: 36,
              fontFamily: "Roboto-BoldItalic",
              color: "#57A6A1",
            }}
          >
            Tạo tài khoản
          </Text>
        </View>
        {/*Login form*/}

        <View
          style={{
            display: "flex",
            gap: 20,
            marginTop: 30,
            width: 380,
            padding: 10,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  width: "100%",
                  gap: 10,
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    gap: 5,
                    flexDirection: "row",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Regular",
                      fontSize: 20,
                      color: "#496989",
                    }}
                  >
                    Tên Người Dùng
                  </Text>
                  <Icon name="edit" size={20} color="#496989" />
                </View>
                <TextInput
                  placeholder="Nhập tên..."
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  style={{
                    width: "100%",
                    height: 60,
                    borderColor: "#57A6A1",
                    borderWidth: 1,
                    borderRadius: 8,
                    padding: 10,
                    fontFamily: "Roboto-Regular",
                    color: "#496989",
                    fontSize: 20,
                  }}
                />
              </View>
            )}
            name="Username"
          />
          {errors.Username && (
            <Text
              style={{
                color: "#FF4C4C",
                fontFamily: "Roboto-Regular",
                marginTop: -10,
              }}
            >
              Vui lòng nhập tên của bạn !
            </Text>
          )}
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  width: "100%",
                  gap: 10,
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    gap: 5,
                    flexDirection: "row",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Regular",
                      fontSize: 20,
                      color: "#496989",
                    }}
                  >
                    Địa chỉ
                  </Text>
                  <Icon
                    name="map-marker-alt"
                    size={15}
                    type="font-awesome-5"
                    color="#496989"
                  />
                </View>
                <TextInput
                  placeholder="Nhập địa chỉ..."
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  style={{
                    width: "100%",
                    height: 60,
                    borderColor: "#57A6A1",
                    borderWidth: 1,
                    borderRadius: 8,
                    padding: 10,
                    fontFamily: "Roboto-Regular",
                    color: "#496989",
                    fontSize: 20,
                  }}
                />
              </View>
            )}
            name="address"
          />
          {errors.address && (
            <Text
              style={{
                color: "#FF4C4C",
                fontFamily: "Roboto-Regular",
                marginTop: -10,
              }}
            >
              Vui lòng nhập địa chỉ của bạn !
            </Text>
          )}
          <Controller
            control={control}
            rules={{
              required: true,
              pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  width: "100%",
                  gap: 10,
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    gap: 5,
                    flexDirection: "row",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Regular",
                      fontSize: 20,
                      color: "#496989",
                    }}
                  >
                    Email liên hệ
                  </Text>
                  <Icon name="email" size={15} color="#496989" />
                </View>

                <TextInput
                  placeholder="Nhập email"
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  style={{
                    width: "100%",
                    height: 60,
                    borderColor: "#57A6A1",
                    borderWidth: 1,
                    borderRadius: 8,
                    padding: 10,
                    fontFamily: "Roboto-Regular",
                    color: "#496989",
                    fontSize: 20,
                  }}
                />
              </View>
            )}
            name="Email"
          />
          {errors.Email && (
            <Text style={{ color: "red" }}>
              Email không hợp lệ hoặc đã được đăng kí !
            </Text>
          )}
          <Controller
            control={control}
            rules={{
              minLength: 9,
              required: true,
              pattern: /^[0-9]+$/,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  width: "100%",
                  gap: 10,
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    gap: 5,
                    flexDirection: "row",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Regular",
                      fontSize: 20,
                      color: "#496989",
                    }}
                  >
                    Số điện thoại
                  </Text>
                  <Icon name="call" size={15} color="#496989" />
                </View>

                <TextInput
                  placeholder="Nhập số điện thoại"
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  style={{
                    width: "100%",
                    height: 60,
                    borderColor: "#57A6A1",
                    color: "#496989",
                    borderWidth: 1,
                    borderRadius: 8,
                    padding: 10,
                    fontFamily: "Roboto-Regular",
                    fontSize: 20,
                  }}
                />
              </View>
            )}
            name="phonenumber"
          />
          {errors.phonenumber && (
            <Text style={{ color: "red" }}>
              Số điện thoại không hợp lệ hoặc chưa đăng ký !
            </Text>
          )}
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  width: "100%",
                  gap: 10,
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    gap: 5,
                    flexDirection: "row",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Regular",
                      fontSize: 20,
                      color: "#496989",
                    }}
                  >
                    Mật khẩu
                  </Text>
                  <Icon name="lock" size={15} color="#496989" />
                </View>

                <TextInput
                  placeholder="Nhập mật khẩu"
                  onBlur={onBlur}
                  onChangeText={onChange}
                  secureTextEntry={true}
                  value={value}
                  style={{
                    width: "100%",
                    height: 60,
                    borderColor: "#57A6A1",
                    borderWidth: 1,
                    borderRadius: 8,
                    padding: 10,
                    fontFamily: "Roboto-Regular",
                    color: "#496989",
                    fontSize: 20,
                  }}
                />
              </View>
            )}
            name="password"
          />
          {errors.password && (
            <Text style={{ color: "red" }}>Mật khẩu không hợp lệ !</Text>
          )}

          <Controller
            control={control}
            name="image"
            rules={{ required: true }}
            render={({ field: { onChange, value } }) => (
              <View
                style={{
                  display: "flex",
                  justifyContent: "center",
                  width: "100%",
                  maxWidth: 392,
                  alignItems: "center",
                  gap: 10,
                }}
              >
                <Button
                  title="Chọn Ảnh đại diện"
                  onPress={async () => {
                    try {
                      const { status } =
                        await ImagePicker.requestMediaLibraryPermissionsAsync();
                      if (status !== "granted") {
                        alert(
                          "Sorry, we need camera roll permissions to make this work!"
                        );
                        return;
                      }
                      let result = await ImagePicker.launchImageLibraryAsync({
                        mediaTypes: ImagePicker.MediaTypeOptions.All,
                        allowsEditing: true,
                        aspect: [4, 3],
                        quality: 1,
                      });

                      if (!result.canceled) {
                        // setImage(result.assets[0].uri);
                        onChange(result.assets[0].uri);
                      }
                    } catch (error) {
                      // No permissions request is necessary for launching the image library
                      console.erorr("Error picking image: ", error);
                    }
                  }}
                  buttonStyle={{
                    backgroundColor: "white",
                    width: 392,
                    maxWidth: "100%",
                    borderColor: "#57A6A1",
                    borderWidth: 1,

                    height: 68,
                    borderRadius: 12,
                  }}
                  titleStyle={{
                    fontSize: 20,
                    color: "#496989",
                    fontFamily: "Roboto-Medium",
                  }}
                ></Button>
                {value && (
                  <Image
                    source={{ uri: value }}
                    style={{ width: 200, height: 200, borderRadius: 100 }}
                  />
                )}
              </View>
            )}
          />
          {errors.image && (
            <Text style={{ color: "red" }}>Hãy up ảnh đại diện !</Text>
          )}
          <Button
            title="Tạo tài khoản"
            onPress={handleSubmit(onSubmit)}
            buttonStyle={{
              backgroundColor: "#57A6A1",
              width: 391,
              maxWidth: "100%",
              height: 68,
              borderRadius: 12,

              marginBottom: 20,
            }}
            titleStyle={{
              fontSize: 20,
              fontWeight: "bold",
            }}
          />
        </View>
        <Text
          style={{
            color: "#496989",
            marginTop: 20,
            fontSize: 20,
            fontFamily: "Roboto",
            marginBottom: 50,
          }}
          onPress={() => navigation.navigate("LoginScreen", {})}
        >
          Quay về trang đăng nhập
        </Text>
      </ScrollView>
    </View>
  );
};
export default SignUpScreen;
