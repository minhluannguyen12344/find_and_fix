import React, { useEffect, useRef, useState, useCallback } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { Header, Icon } from "@rneui/themed";
import axios from "axios";
import MapView, { Polyline, Geojson } from "react-native-maps";
import * as Location from "expo-location";
import Toast from "react-native-toast-message";
import { useFonts } from "expo-font";
import { get } from "react-native/Libraries/TurboModule/TurboModuleRegistry";

const MAPBOX_ACCESS_TOKEN =
  "pk.eyJ1IjoibHVrZXZpcnVzMTIzMCIsImEiOiJjbHl1M2M5OGYwaGMyMmlzMHJnNTh2cWNqIn0.fbySaOq3v-PloYxqvsXzHA";
const DIRECTIONS_API_BASE_URL =
  "https://api.mapbox.com/directions/v5/mapbox/driving/";

const NavigateMap = ({ route, navigation }) => {
  const errorToast = () => {
    Toast.show({
      type: "error",
      text1: "Địa chỉ không chính xác hãy báo cho chúng tôi",
      position: "top",
    });
  };
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const { area } = route.params;
  // const area = "Quận 1, Hồ Chí Minh";
  const [location, setLocation] = useState(null);
  const [servicesDestination, setServicesDestination] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [getDistance, setGetDistance] = useState(null);
  const [duration, setDuration] = useState(null);
  const [routeCoordinates, setRouteCoordinates] = useState([]);
  const mapRef = useRef(null);
  // Get access to user's location
  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }
      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, [area]);

  // Fetch services destination address
  const fetchDestination = useCallback(async () => {
    try {
      const response = await axios.get(
        `https://api.mapbox.com/search/geocode/v6/forward?q=${encodeURIComponent(
          area
        )}&access_token=${MAPBOX_ACCESS_TOKEN}`
      );
      const destination = response.data.features[0].geometry.coordinates;
      setServicesDestination({
        latitude: destination[1],
        longitude: destination[0],
      });
    } catch (error) {
      errorToast();
    }
  }, [area]);
  // Call fetchDestination when the component mounts
  useEffect(() => {
    fetchDestination();
  }, [fetchDestination]);
  // Fetch directions from user's location to services destination
  const fetchDirections = useCallback(async () => {
    if (!location || !servicesDestination) return;
    const origin = `${location.coords.longitude},${location.coords.latitude}`;
    const destination = `${servicesDestination.longitude},${servicesDestination.latitude}`;
    const url = `${DIRECTIONS_API_BASE_URL}${origin};${destination}?geometries=geojson&access_token=${MAPBOX_ACCESS_TOKEN}`;
    try {
      const response = await axios.get(url);
      const coordinates = response.data.routes[0].geometry.coordinates;
      const points = coordinates.map((coord) => ({
        latitude: coord[1],
        longitude: coord[0],
      }));
      setRouteCoordinates(points);
      setGetDistance(response.data.routes[0].distance);
      setDuration(response.data.routes[0].duration);
    } catch (error) {
      errorToast();
    }
  }, [location, servicesDestination]);
  // Call fetchDirections when the component mounts
  useEffect(() => {
    fetchDirections();
  }, [fetchDirections]);
  // wait for location to be fetched
  let textLocation = errorMsg
    ? errorMsg
    : location
    ? JSON.stringify(location)
    : "Waiting..";
  // Add marker for user's location
  const myPlace = {
    type: "FeatureCollection",
    features: [
      {
        type: "Feature",
        properties: {},
        geometry: {
          type: "Point",
          coordinates: [
            `${location?.coords.longitude}`,
            `${location?.coords.latitude}`,
          ],
        },
      },
    ],
  };
  return (
    <View style={styles.container}>
      <Header
        containerStyle={{
          height: "10%",
        }}
        backgroundColor="#57A6A1"
        leftComponent={
          <Icon
            type="font-awesome-5"
            name="chevron-left"
            color="white"
            size={25}
            onPress={() => navigation.goBack()}
          />
        }
        centerComponent={{
          text: "Vị trí dịch vụ",
          style: { color: "#fff", fontSize: 20, fontWeight: "bold" },
        }}
      />

      <MapView
        style={styles.map}
        ref={mapRef}
        initialRegion={{
          latitude: 10.762622,
          longitude: 106.660172,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      >
        <Geojson
          geojson={myPlace}
          strokeColor="red"
          fillColor="green"
          strokeWidth={2}
        />
        {routeCoordinates.length > 0 && (
          <Polyline
            coordinates={routeCoordinates}
            strokeWidth={2}
            strokeColor="red"
          />
        )}
      </MapView>
      <View
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "flex-start",
          paddingLeft: 40,
          paddingTop: 10,
          paddingBottom: 10,
          borderTopLeftRadius: 40,
          borderTopRightRadius: 40,
          position: "absolute",
          bottom: 0,
          left: 0,
          right: 0,
          backgroundColor: "#FCF8F3",
          height: 110,
          width: "100%",
        }}
      >
        <View
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            gap: 5,
            height: "auto",
          }}
        >
          <Text
            style={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 16,
            }}
          >
            Địa chỉ dịch vụ:
          </Text>
          <Text
            style={{
              color: "#496989",
              fontFamily: "Roboto-Medium",
              fontSize: 16,
            }}
          >
            {"  "}
            {area}
          </Text>
        </View>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: 5,
          }}
        >
          <Icon name="social-distance" color="#45474B" />
          <Text
            style={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 16,
            }}
          >
            Khoảng cách:
          </Text>

          <Text
            style={{
              color: "#496989",
              fontFamily: "Roboto-Medium",
              fontSize: 16,
            }}
          >
            {"  "}
            {Math.round(getDistance / 1000)} km
          </Text>
        </View>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: 5,
          }}
        >
          <Icon name="time" type="ionicon" color="#45474B" />
          <Text
            style={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 16,
            }}
          >
            Thời gian:
          </Text>

          <Text
            style={{
              color: "#496989",
              fontFamily: "Roboto-Medium",
              fontSize: 16,
            }}
          >
            {"  "}
            {`${Math.round(duration / 3600)}h ${Math.round(
              (duration % 3600) / 60
            )}m ${Math.round(duration % 60)}s`}
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  map: { width: "100%", height: 680 },
});

export default NavigateMap;
