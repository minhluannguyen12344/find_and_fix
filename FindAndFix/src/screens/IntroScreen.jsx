import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, Image, ActivityIndicator } from "react-native";
import { Header, Button } from "@rneui/themed";
import intro_bg from "../../asset/imgs/intro_bg.png";
import { useFonts } from "expo-font";

const IntroScreen = ({ navigation }) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white",
        height: "100%",
      }}
    >
      <Header
        backgroundColor="#57A6A1"
        containerStyle={{
          width: "100%",
          height: 120,
          marginTop: -55,
        }}
      ></Header>
      <Image
        source={intro_bg}
        style={{
          width: "100%",
          height: 287,
          marginTop: 50,
          marginBottom: 50,
        }}
      />
      <Text
        style={{
          color: "#57A6A1",
          fontSize: 24,
          lineHeight: 32,
          margin: 20,
          width: 300,
          textAlign: "center",
          fontFamily: "Roboto-Bold",
        }}
      >
        Chào mừng bạn đến với ứng dụng Find & Fix
      </Text>
      <Button
        title="Bắt đầu"
        buttonStyle={{
          backgroundColor: "#57A6A1",
          width: 350,
          height: 68,
          borderRadius: 12,
          boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
          margin: 20,
        }}
        titleStyle={{
          fontSize: 24,

          fontFamily: "Roboto-Medium",
        }}
        onPress={() => navigation.navigate("LoginScreen")}
      />
    </View>
  );
};
export default IntroScreen;
