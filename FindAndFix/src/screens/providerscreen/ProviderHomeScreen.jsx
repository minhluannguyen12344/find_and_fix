import { Text, View, Image, ScrollView } from "react-native";
import AppBar from "../../components/AppBar";
import { useRoute } from "@react-navigation/native";
import { Header, Button, Dialog } from "@rneui/themed";
//import component
import DialogAddServices from "../../components/DialogAddServices";
import { useEffect, useState } from "react";
import axios from "axios";
import ProviderServices from "../../components/ProviderServices";
//import redux
import { useDispatch, useSelector } from "react-redux";
import { updateUserInfo } from "../../config/reduxStore";
//import local hostIp
import localHostEnv from "../../config/localhost_env";

const ProviderHomeScreen = ({ navigation }) => {
  const route = useRoute();
  const { userId } = route.params;
  const dispatch = useDispatch();
  const [provideServices, setProviderServices] = useState([]);
  // Get Provider info
  useEffect(() => {
    const getUserInfo = async () => {
      try {
        const response = await axios.get(`${localHostEnv}/user/${userId}`);
        // setUser(response.data);
        dispatch(updateUserInfo(response.data));
        console.log("provider", response.data);
      } catch (e) {
        console.error("Failed to fetch user data:", e);
      }
    };
    getUserInfo();
  }, [userId, dispatch]);
  // Get Provider services
  useEffect(() => {
    const getServices = async () => {
      try {
        const response = await axios.get(
          `${localHostEnv}/services/provider/${userId}`
        );
        setProviderServices(response.data);
        console.log("services", response.data);
      } catch (e) {
        console.error("Failed to fetch services data:", e);
      }
    };
    getServices();
  }, [userId]);
  // Dialog for add service

  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-start",
        backgroundColor: "#EEEDEB",

        flexDirection: "column",
      }}
    >
      <AppBar />
      <ScrollView
        style={{
          width: "100%",
        }}
        contentContainerStyle={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          marginTop: 40,
          gap: 20,
        }}
      >
        <DialogAddServices />

        <ProviderServices userId={userId} />
      </ScrollView>
    </View>
  );
};

export default ProviderHomeScreen;
