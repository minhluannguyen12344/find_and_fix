import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
} from "react-native";
import { Rating } from "react-native-ratings";
import React, { useEffect, useState } from "react";
import { Header, Icon, Button } from "@rneui/themed";
import localHostEnv from "../../config/localhost_env";
import Comment from "../../components/Comment";
import axios from "axios";
import { useFonts } from "expo-font";

import { useDispatch, useSelector } from "react-redux";
const ProviderDetailServicesScreen = ({ route, navigation }) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const {
    servicesId,
    providerId,
    servicesName,
    servicesDescription,
    servicesThumbnail,
    servicesPrice,
    servicesArea,
    servicesExperience,
  } = route.params;
  const [comments, setComments] = useState([]);
  const userInfo = useSelector((state) => state.user.userInfo);
  console.log("servicesId", servicesId);
  useEffect(() => {
    const getCommentData = async () => {
      try {
        const response = await axios.get(
          `${localHostEnv}/comment/${servicesId}`
        );
        setComments(response.data);
        console.log("commentInfo", response.data);
      } catch (e) {
        console.error("Load dữ liệu đánh giá lỗi", e);
      }
    };
    getCommentData();
  }, []);

  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        backgroundColor: "#EEEDEB",
      }}
    >
      <Header
        backgroundColor="#57A6A1"
        containerStyle={{
          height: 100,
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: 5,
        }}
        leftComponent={
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Icon
              type="font-awnsome-5"
              name="chevron-left"
              color="white"
              size={50}
              containerStyle={{
                marginTop: -10,
              }}
              onPress={() => navigation.goBack()}
            />
          </View>
        }
        centerComponent={
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: 20,
                fontWeight: "bold",
              }}
            >
              Chi tiết dịch vụ
            </Text>
          </View>
        }
      />
      <ScrollView showsVerticalScrollIndicator={false} horizontal={false}>
        {/* UI cho tiết dịch vụ */}
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-start",
            width: "100%",
            gap: 20,
            marginTop: 20,
            height: "auto",

            backgroundColor: "#FCF8F3",
            flexDirection: "column",
            borderRadius: 12,
          }}
        >
          <Image
            source={{
              uri: `${servicesThumbnail}`,
            }}
            style={{
              width: "100%",
              height: 300,
              objectFit: "cover",
              borderTopLeftRadius: 12,
              borderTopRightRadius: 12,
            }}
          />
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",

              marginRight: 10,
            }}
          >
            <Text
              style={{
                color: "#496989",
                fontFamily: "Roboto-BoldItalic",
                fontSize: 30,
              }}
            >
              {"  "}
              {servicesName}
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              marginLeft: 10,
              marginRight: 10,
              height: "auto",
              width: "100%",
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                flexDirection: "row",
                width: "100%",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 20,
                }}
              >
                Thông tin chi tiết
              </Text>
              <Icon name="book" size={20} />
            </View>
            <Text
              style={{
                multiLine: true,
                width: 350,

                color: "#496989",
                fontFamily: "Roboto",
                fontSize: 18,
              }}
            >
              {servicesDescription}
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              marginLeft: 10,
              marginRight: 10,
              height: "auto",
              width: "100%",
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                flexDirection: "row",
                width: "100%",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 20,
                }}
              >
                kinh nghiệm làm việc
              </Text>
            </View>
            <Text
              style={{
                multiLine: true,
                width: 350,

                color: "#496989",
                fontFamily: "Roboto",
                fontSize: 18,
              }}
            >
              {servicesExperience}
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
              marginLeft: 10,
              marginRight: 10,
              height: "auto",
              width: "100%",
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                flexDirection: "row",
                width: "100%",
              }}
            >
              <Text
                style={{
                  color: "#45474B",
                  fontFamily: "Roboto-Bold",
                  fontSize: 20,
                }}
              >
                Địa chỉ dịch vụ
              </Text>
              <Icon name="location-pin" size={20} color="#FF4C4C" />
            </View>
            <Text
              style={{
                multiLine: true,
                width: 350,

                color: "#496989",
                fontFamily: "Roboto",
                fontSize: 18,
              }}
            >
              {servicesArea}
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              marginLeft: 10,
              marginRight: 10,
              height: "auto",
              gap: 5,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <Text
                style={{
                  fontSize: 30,
                  fontFamily: "Roboto-Bold",
                  color: "#45474B",
                }}
              >
                Giá tiền:
              </Text>
            </View>
            <Text
              style={{
                color: "#E4003A",
                fontFamily: "Roboto-bold",
                fontSize: 30,
              }}
            >
              {servicesPrice}
            </Text>
            <Text
              style={{
                color: "#496989",
                textDecorationLine: "underline",
                fontFamily: "Roboto-BoldItalic",
                fontSize: 30,
              }}
            >
              .đ
            </Text>
          </View>
        </View>

        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            gap: 20,
            marginTop: 20,
            padding: 10,
            backgroundColor: "#FCF8F3",
            borderRadius: 12,
            marginBottom: 80,
          }}
        >
          <Text
            style={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 20,
              marginTop: 20,
            }}
          >
            Đánh giá khách hàng:
          </Text>
          {comments.map((comment, index) => (
            <View
              key={index}
              style={{
                width: "100%",
                height: "auto",
                borderRadius: 12,
                borderColor: "#EEEEEE",
                backgroundColor: "#F0EBE3",
                borderWidth: 1,
                padding: 10,
                display: "flex",
                gap: 20,
                justifyContent: "center",
                alignItems: "flex-start",
              }}
            >
              <View
                style={{
                  display: "flex",
                  width: "100%",
                  justifyContent: "space-between",
                  alignItems: "center",
                  flexDirection: "row",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "row",
                  }}
                >
                  <Image
                    style={{
                      width: 50,
                      height: 50,
                      borderRadius: 50,
                      marginRight: 10,
                    }}
                    source={{
                      uri: `${localHostEnv}/${comment.user.avatar.replace(
                        /\\/g,
                        "/"
                      )}`,
                    }}
                  />
                  <View>
                    <Text
                      style={{
                        color: "#4D869C",
                        fontWeight: "bold",
                        fontSize: 20,
                      }}
                    >
                      {comment.user.fullname}
                    </Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "flex-start",
                  flexDirection: "column",
                  gap: 10,
                }}
              >
                <Rating
                  readonly
                  startingValue={comment.rating}
                  imageSize={20}
                  tintColor="#F0EBE3"
                />
                {/* <Text
                  style={{
                    color: "#4D869C",
                    fontWeight: "bold",
                    fontSize: 18,
                  }}
                >
                  Nhận xét :
                </Text> */}
                <Text
                  style={{
                    color: "grey",
                    fontWeight: "bold",
                    fontSize: 16,
                  }}
                >
                  {comment.content}
                </Text>
              </View>
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default ProviderDetailServicesScreen;
