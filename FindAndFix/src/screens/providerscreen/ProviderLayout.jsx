// @ts-nocheck
import { Text, View } from "react-native";
//Tabs Screens

import ProviderHomeScreen from "./ProviderHomeScreen";
import ProviderOrderScreen from "./ProviderOrderScreen";
import ProviderNotification from "./ProviderNotification";
import ProviderInfoScreen from "./ProviderInfoScreen";
import UserInfoScreen from "../userscreen/UserInfoScreen";
//Components library
import { Icon } from "@rneui/themed";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const Tab = createBottomTabNavigator();
const ProviderLayout = ({ route, navigation }) => {
  const { userId } = route.params;
  console.log("ProviderLayout", userId);
  return (
    <Tab.Navigator
      headerShow={false}
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarStyle: { backgroundColor: "#57A6A1", height: 60, gap: 10 },
        tabBarActiveTintColor: "#005C78",
        tabBarInactiveTintColor: "white",

        tabBarIcon: ({ color, size }) => {
          let iconName;
          if (route.name === "ProviderHome") {
            iconName = "home";
          } else if (route.name === "ProviderOrder") {
            iconName = "list-ul";
          } else if (route.name === "ProviderNotification") {
            iconName = "bell";
          } else if (route.name === "Info") {
            iconName = "user";
          }

          return (
            <Icon
              name={iconName}
              size={size}
              color={color}
              type="font-awesome-5"
            />
          );
        },
        tabBarLabel: ({ focused, color, size }) => {
          let labelName;
          if (route.name === "ProviderHome") {
            labelName = "Trang chủ";
          } else if (route.name === "ProviderOrder") {
            labelName = "Hoạt động";
          } else if (route.name === "ProviderNotification") {
            labelName = "Thông báo";
          } else if (route.name === "Info") {
            labelName = "Thông tin";
          }
          return (
            <Text style={{ color: color, fontSize: size }}>{labelName}</Text>
          );
        },
      })}
    >
      <Tab.Screen
        name="ProviderHome"
        component={ProviderHomeScreen}
        initialParams={{ userId: userId }}
      />
      <Tab.Screen
        name="ProviderOrder"
        component={ProviderOrderScreen}
        initialParams={{ userId: userId }}
      />
      <Tab.Screen
        name="ProviderNotification"
        component={ProviderNotification}
        initialParams={{ userId: userId }}
      />
      <Tab.Screen
        name="Info"
        component={UserInfoScreen}
        initialParams={{ userId: userId }}
      />
    </Tab.Navigator>
  );
};

export default ProviderLayout;
