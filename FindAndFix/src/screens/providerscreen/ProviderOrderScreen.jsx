import { Text, View, ScrollView, Image } from "react-native";
import AppBar from "../../components/AppBar";
import { Tab, Button, Dialog, Icon } from "@rneui/themed";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";
import localHostEnv from "../../config/localhost_env";
import axios from "axios";
import DropDownPicker from "react-native-dropdown-picker";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { useFonts } from "expo-font";

const statusLabel = [
  { label: "Đang chờ xử Lý", value: "pending" },
  { label: "Đang thực hiện", value: "onDoing" },
  { label: "Hoàn thành", value: "Complete" },
  { label: "Hủy Đơn", value: "Cancel" },
];

const ProviderOrderScreen = () => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const userInfo = useSelector((state) => state.user.userInfo);
  const [orderData, setOrderData] = useState([]);
  const [reload, setReload] = useState(false);
  useEffect(() => {
    const getOrderData = async () => {
      try {
        const response = await axios.get(
          `${localHostEnv}/order/provider/${userInfo._id}`
        );
        setOrderData(response.data);
      } catch (e) {
        console.error("Failed to fetch order data:", e);
      }
    };
    getOrderData();
  }, [userInfo._id, reload]);
  // update order status
  const [value, setValue] = useState("pending"); // Set a default value for the dropdown
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(!open);

  const [currentOrder, setCurrentOrder] = useState(null);
  const [show, setShow] = useState(false);
  const [status, setStatus] = useState("");
  const updateStatus = (order) => {
    setCurrentOrder(order);
    setShow(!show);
  };
  useEffect(() => {
    if (currentOrder) {
      setValue(currentOrder.status);
    }
  }, [currentOrder]);

  const handleUpdateStatus = async () => {
    try {
      const response = await axios.put(
        `${localHostEnv}/order/${currentOrder._id}`,
        {
          status: value,
        }
      );
      if (response.status === 200) {
        setReload((prev) => !prev);
        const sendEmail = await axios.post(
          `${localHostEnv}/notification/sendEmails`,
          {
            userEmail: currentOrder.userEmail,
            orderID: currentOrder._id,
          }
        );
        console.log(sendEmail);
        setShow(false);
      }
    } catch (e) {
      console.error("Cập nhật trạng thái thất bại:", e);
      setErrorMessage("Cập nhật trạng thái thất bại");
    }
  };
  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <AppBar />

      <ScrollView
        style={{
          width: "100%",
        }}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          gap: 10,
        }}
      >
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#FCF8F3",
            borderRadius: 12,

            width: "95%",
            height: 60,
            padding: 10,
            marginTop: 20,
          }}
        >
          <Text
            style={{
              fontSize: 20,
              fontWeight: "bold",
              color: "#45474B",
            }}
          >
            Đơn hàng đang có
          </Text>
        </View>
        <Button
          title="Làm mới danh sách"
          onPress={() => setReload((prev) => !prev)}
          buttonStyle={{
            backgroundColor: "#F0EBE3",

            width: 180,
            height: 40,
            gap: 10,
            borderRadius: 12,
            borderColor: "grey",
            marginTop: 10,
            borderWidth: 1,
          }}
        >
          <Text
            style={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 20,
            }}
          >
            Làm mới đơn
          </Text>
          <Icon name="reload" size={20} type="ionicon" color="#45474B" />
        </Button>
        <View
          style={{
            width: "100%",
            marginBottom: 100,
            padding: 10,
          }}
        >
          {orderData.map((order, key) => (
            <View
              key={key}
              style={{
                width: "100%",

                marginTop: 20,
                borderRadius: 12,

                backgroundColor: "#FCF8F3",
                gap: 20,
                marginBottom: 30,
              }}
            >
              <View
                style={{
                  width: "100%",
                  height: "auto",
                  backgroundColor: "#F9E2AF",
                  borderTopLeftRadius: 12,
                  borderTopEndRadius: 12,
                  padding: 10,
                }}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "flex-start",
                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      color: "#45474B",

                      fontSize: 16,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    Mã đơn:
                  </Text>
                  <Text
                    style={{
                      color: "#496989",
                      fontWeight: "bold",
                      fontSize: 16,
                    }}
                  >
                    {order._id}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Ngày xử Lý:
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontFamily: "Roboto-Bold",
                    fontSize: 16,
                  }}
                >
                  {new Date(order.bookingDate).toLocaleString("vi-VN")}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Ngày đặt:{" "}
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontFamily: "Roboto-Bold",
                    fontSize: 16,
                  }}
                >
                  {new Date(order.createdAt).toLocaleString("vi-VN")}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Số điện thoại:
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  {order.phone}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-end",
                  paddingLeft: 10,
                  paddingRight: 10,
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    color: "#45474B",

                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Tên người đặt
                </Text>
                <Text
                  style={{
                    color: "#496989",

                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  {order.name}
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Trạng thái:{" "}
                </Text>
                <Text
                  style={{
                    color: "grey",
                    fontSize: 16,
                  }}
                >
                  {order.status === "pending" ? (
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        backgroundColor: "#ECB159",
                        alignItems: "center",
                        padding: 5,
                        borderRadius: 100,
                        gap: 5,
                      }}
                    >
                      <MaterialIcons name="schedule" size={20} color="white" />
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "Roboto-Bold",
                        }}
                      >
                        Đang chờ xử lý
                      </Text>
                    </View>
                  ) : order.status === "onDoing" ? (
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "#478CCF",
                        borderRadius: 100,
                        padding: 5,
                        gap: 5,
                      }}
                    >
                      <MaterialIcons name="output" size={20} color="white" />
                      <Text
                        style={{
                          fontFamily: "Roboto-Bold",
                          color: "white",
                        }}
                      >
                        Đang tiến hành
                      </Text>
                    </View>
                  ) : order.status === "Complete" ? (
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "#88D66C",
                        borderRadius: 100,
                        gap: 5,
                        padding: 5,
                      }}
                    >
                      <MaterialIcons name="check" size={20} color="white" />
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "Roboto-Bold",
                        }}
                      >
                        Hoàn Thành
                      </Text>
                    </View>
                  ) : order.status === "Cancel" ? (
                    <View
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "#FF4C4C",
                        borderRadius: 100,
                        padding: 5,
                        gap: 5,
                      }}
                    >
                      <MaterialIcons name="delete" size={20} color="white" />
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "Roboto-Bold",
                        }}
                      >
                        Hủy đơn
                      </Text>
                    </View>
                  ) : null}
                </Text>
              </View>
              <View
                style={{
                  width: "100%",
                  height: 1,
                  backgroundColor: "grey",
                }}
              ></View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  gap: 5,
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",

                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Loại dịch vụ đặt:{" "}
                </Text>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 10,
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}
                >
                  <Image
                    style={{
                      width: 100,
                      height: 100,
                      borderRadius: 12,
                    }}
                    source={{
                      uri: `${localHostEnv}/${order.services[0].thumbnail.replace(
                        /\\/g,
                        "/"
                      )}`,
                    }}
                  ></Image>
                  <Text
                    style={{
                      color: "#496989",
                      fontSize: 20,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    {order.services[0].servicesName}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  justifyContent: "center",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Địa chỉ xử lý
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontFamily: "Roboto-Bold",
                    fontSize: 16,
                  }}
                >
                  {order.address}
                </Text>
              </View>

              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Ghi chú:
                </Text>
                <Text
                  style={{
                    color: "#496989",
                    fontSize: 16,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  {order.note}
                </Text>
              </View>
              <View
                style={{
                  width: "100%",
                  backgroundColor: "grey",
                  height: 1,
                }}
              ></View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "flex-end",
                  paddingLeft: 10,
                  paddingRight: 10,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",

                    fontSize: 25,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Tổng tiền:{" "}
                </Text>
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "row",
                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      color: "#FF4C4C",
                      fontSize: 25,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    {order.price}
                  </Text>
                  <Text
                    style={{
                      color: "#496989",
                      textDecorationLine: "underline",
                      fontSize: 16,
                      fontFamily: "Roboto-BoldItalic",
                    }}
                  >
                    đ
                  </Text>
                </View>
              </View>
              <Button
                title="Điều chỉnh Trạng Thái"
                onPress={() => {
                  updateStatus(order);
                }}
                buttonStyle={{
                  backgroundColor: "#4D869C",
                  width: "auto",
                  height: 40,
                  borderRadius: 12,
                  borderColor: "#57A6A1",
                  borderWidth: 1,
                  marginTop: 20,
                }}
              />
              <Dialog
                visible={show}
                onDismiss={() => setShow(false)}
                overlayStyle={{
                  display: "flex",
                  width: 350,
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 12,
                  position: "relative",
                }}
              >
                <Dialog.Title
                  title="Cập trạng thái đơn hàng"
                  titleStyle={{
                    color: "#57A6A1",
                    fontSize: 24,
                    fontWeight: "bold",
                  }}
                />
                {currentOrder && (
                  <View
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      flexDirection: "column",
                      height: "auto",
                      gap: 20,
                      position: "relative",
                    }}
                  >
                    <View
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        flexDirection: "row",
                        gap: 5,
                      }}
                    >
                      <View
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          gap: 3,
                        }}
                      >
                        <Icon
                          type="font-awesome-5"
                          name="ticket-alt"
                          color="#B67352"
                          size={20}
                        />
                        <Text
                          style={{
                            fontSize: 16,
                            fontWeight: "bold",
                            color: "#57A6A1",
                          }}
                        >
                          Mã đơn:
                        </Text>
                      </View>

                      <Text
                        style={{
                          color: "#496989",
                          fontWeight: "bold",
                        }}
                      >
                        {currentOrder._id}
                      </Text>
                    </View>
                    <View
                      style={{
                        position: "relative",
                        zIndex: 1000,
                      }}
                    >
                      <DropDownPicker
                        style={{
                          backgroundColor: "#8CB9BD", // Set a light gray background
                          padding: 10,
                          borderColor: "#496989",
                          borderWidth: 1,
                        }}
                        textStyle={{
                          color: "#496989",
                          fontWeight: "bold",
                        }}
                        open={open}
                        value={value} // Set the initial value to the default
                        items={statusLabel} // Set the items for the dropdown
                        onOpen={handleOpen} // Handle opening the dropdown (optional)
                        onClose={() => setOpen(false)} // Handle closing the dropdown (optional)
                        setValue={setValue}
                        // Not typically used in this scenario
                        placeholder={"Chọn trạng thái"}
                      />
                    </View>
                    <View
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        flexDirection: "row",
                        width: "100%",
                        position: "relative",
                        zIndex: 200,
                      }}
                    >
                      <Button
                        buttonStyle={{
                          backgroundColor: "#4D869C",
                          width: 130,
                          height: 40,
                          borderRadius: 12,
                          borderColor: "#57A6A1",
                          borderWidth: 1,
                          marginTop: 20,
                        }}
                        onPress={() => handleUpdateStatus()}
                      >
                        Cập nhật
                      </Button>
                      <Button
                        buttonStyle={{
                          backgroundColor: "#FF4C4C",
                          width: 130,
                          height: 40,
                          borderRadius: 12,

                          marginTop: 20,
                        }}
                        onPress={() => setShow(false)}
                      >
                        Bỏ
                      </Button>
                    </View>
                  </View>
                )}
              </Dialog>
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default ProviderOrderScreen;
