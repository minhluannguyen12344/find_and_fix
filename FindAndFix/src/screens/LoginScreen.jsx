import {
  Text,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Keyboard,
  ScrollView,
} from "react-native";
import { Button } from "@rneui/themed";
import app_icon from "../../asset/imgs/app_icon.png";
import { useForm, Controller } from "react-hook-form";
import { Icon } from "@rneui/themed";
import google_icon from "../../asset/imgs/google_icon.png";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useFonts } from "expo-font";
// backend fetch
import axios from "axios";
import localHostEnv from "../config/localhost_env";
const Login = ({ navigation }) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({});
  const onSubmit = async (data) => {
    try {
      const response = await axios.post(
        // `http://${localHostEnv}:3000/auth/signin`,
        `${localHostEnv}/auth/signin`,
        {
          email: data.Email,
          password: data.password,
        }
      );
      const userId = response.data.data._id;
      const userRole = response.data.data.role;
      const token = response.data.token;
      await storeUserData(userId, userRole, token);
      console.log(userId, userRole);
      if (userRole === "user") {
        navigation.navigate("UserMainLayout", { userId: userId });
      } else {
        navigation.navigate("ProviderLayout", { userId: userId });
      }
    } catch (error) {
      if (error.response && error.response.status === 500) {
        console.error(error.response.data.content);
      } else {
        console.error(error);
      }
    }
  };
  const storeUserData = async (userId, userRole, token) => {
    try {
      await AsyncStorage.multiSet([
        ["userId", userId],
        ["userRole", userRole],
        ["userToken", token],
      ]);
      const storedData = await AsyncStorage.multiGet([
        "userId",
        "userRole",
        "userToken",
      ]);
      console.log("Stored data:", storedData);
    } catch (error) {
      console.error("Error storing the token", error);
    }
  };
  return (
    //Header
    <SafeAreaView
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white",
        height: "100%",
      }}
      onTouchStart={() => Keyboard.dismiss()}
    >
      <ScrollView
        contentContainerStyle={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          width: "100%",
        }}
      >
        {/*Icon App View*/}
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 50,
          }}
        >
          <Image
            source={app_icon}
            style={{
              width: 88,
              height: 81,
            }}
          />
          <Text
            style={{
              fontSize: 36,
              fontFamily: "Roboto-Bold",
              color: "#57A6A1",
            }}
          >
            Find&Fix
          </Text>
        </View>
        {/*Login form*/}

        <View
          style={{
            display: "flex",
            gap: 20,
            marginTop: 30,
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Controller
            control={control}
            rules={{
              required: true,
              pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  width: 390,
                  gap: 10,
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    gap: 5,
                    flexDirection: "row",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Regular",
                      fontSize: 20,
                      color: "#496989",
                    }}
                  >
                    Địa chỉ Email
                  </Text>
                  <Icon name="email" size={20} color="#496989" />
                </View>

                <TextInput
                  placeholder="Example@Gmail.com"
                  placeholderTextColor="#F1F1F1"
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  style={{
                    width: "100%",
                    height: 60,
                    borderColor: "#57A6A1",
                    borderWidth: 1,
                    borderRadius: 8,
                    padding: 10,
                    fontFamily: "Roboto-Regular",
                    color: "#496989",
                    fontSize: 20,
                  }}
                />
              </View>
            )}
            name="Email"
          />
          {errors.Email && (
            <Text
              style={{
                color: "#FF4C4C",
                fontFamily: "Roboto-Regular",
                marginTop: -10,
              }}
            >
              Email không hợp lệ hoặc chưa đăng ký !
            </Text>
          )}

          <Controller
            control={control}
            rules={{
              required: true,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  width: 392,
                  gap: 10,
                  justifyContent: "center",
                }}
              >
                {/* <View
                  style={{
                    width: 78,
                    height: 78,

                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    borderWidth: 1,
                    borderColor: "#57A6A1",

                    borderBottomStartRadius: 12,
                    borderTopStartRadius: 12,
                  }}
                >
                  <Icon name="lock" size={30} color="white" />
                </View> */}
                <View
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    gap: 5,
                    flexDirection: "row",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Regular",
                      fontSize: 20,
                      color: "#496989",
                    }}
                  >
                    Mật Khẩu
                  </Text>
                  <Icon name="lock" size={20} color="#496989" />
                </View>
                <TextInput
                  placeholder="Nhập mật khẩu"
                  placeholderTextColor="#F1F1F1"
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  secureTextEntry={true}
                  contextMenuHidden={true}
                  style={{
                    width: "100%",
                    height: 60,
                    borderColor: "#57A6A1",
                    borderWidth: 1,
                    borderRadius: 8,
                    padding: 20,
                    fontFamily: "Roboto-Regular",
                    color: "#496989",
                    fontSize: 20,
                  }}
                />
              </View>
            )}
            name="password"
          />
          {errors.password && (
            <Text
              style={{
                color: "#FF4C4C",
                fontFamily: "Roboto-Regular",
                marginTop: -10,
              }}
            >
              Mật Khẩu không hợp lệ !
            </Text>
          )}
          <View
            style={{
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 10,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
              borderRadius: 100, // Match the button's borderRadius
            }}
          >
            <Button
              title="Đăng nhập"
              onPress={handleSubmit(onSubmit)}
              buttonStyle={{
                backgroundColor: "#57A6A1",
                width: 391,
                height: 68,
                borderRadius: 100,
              }}
              titleStyle={{
                fontSize: 20,
                fontFamily: "Roboto-Bold",
              }}
            />
          </View>
          <Text
            style={{
              color: "#496989",
              fontSize: 20,
              fontWeight: "bold",
              marginTop: 30,
              fontFamily: "Roboto-Bold",
            }}
          >
            -------------Hoặc đăng nhập với-------------
          </Text>
          <View
            style={{
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 10,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
              borderRadius: 100, // Match the button's borderRadius
            }}
          >
            <Button
              buttonStyle={{
                width: 370,
                height: 70,
                borderRadius: 100,
                borderColor: "#57A6A1",
                backgroundColor: "#EEEDEB",
                // borderWidth: 1,
                margin: 20,
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  gap: 10,
                }}
              >
                <Image source={google_icon} size={30} />
                <Text
                  style={{
                    color: "#758694",
                    fontSize: 20,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Tài Khoảng google
                </Text>
              </View>
            </Button>
          </View>
        </View>

        <View
          style={{
            display: "flex",
            flexDirection: "row",
            marginBottom: 30,
            width: "100%",
          }}
        >
          <Text
            style={{
              color: "#496989",
              fontSize: 15,

              marginTop: 30,
              fontFamily: "Roboto-Bold",
            }}
          >
            Bạn chưa có tài khoản ?
          </Text>

          <Text
            style={{
              color: "#57A6A1",
              fontSize: 15,
              marginLeft: 15,
              marginTop: 30,
              fontFamily: "Roboto-Bold",
            }}
            onPress={() => navigation.navigate("ChooseRole")}
          >
            Hãy tạo ngay.
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default Login;
