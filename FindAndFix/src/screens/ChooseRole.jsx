import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import app_icon from "../../asset/imgs/app_icon.png";

import { CheckBox, Button, Icon } from "@rneui/themed";
import { useFonts } from "expo-font";

//import img role
import user_role from "../../asset/imgs/user_role.jpg";
import provider_role from "../../asset/imgs/provider_role.jpg";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Keyboard,
  ScrollView,
} from "react-native";
const ChooseRole = ({ navigation }) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const [selectedIndex, setIndex] = useState("user");
  return (
    <SafeAreaView
      onTouchStart={() => Keyboard.dismiss()}
      style={{
        backgroundColor: "white",
        height: "100%",
      }}
    >
      <ScrollView>
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 50,
          }}
        >
          <Image
            source={app_icon}
            style={{
              width: 88,
              height: 81,
            }}
          />
          <Text
            style={{
              fontSize: 36,
              fontFamily: "Roboto-Bold",
              color: "#57A6A1",
            }}
          >
            Find&Fix
          </Text>
          <Text
            style={{
              fontSize: 28,
              fontFamily: "Roboto-Medium",
              color: "grey",
              width: "auto",
              textAlign: "center",
              maxWidth: 350,
              marginTop: 20,
            }}
          >
            Chọn loại tài khoản người dùng
          </Text>
          {/*Chọn loại tài khoản<=====================================================>*/}
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              width: "full",
              marginTop: 20,
              gap: 40,
            }}
          >
            <View
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
                gap: 10,
              }}
            >
              <Icon name="person" size={30} color="#496989" />

              <Text
                style={{
                  fontSize: 20,
                  color: "#496989",
                  fontFamily: "Roboto-Italic",
                }}
              >
                Người dùng
              </Text>
              <Image
                source={user_role}
                style={{ width: 150, height: 150, borderRadius: 12 }}
              />
              <CheckBox
                checked={selectedIndex === "user"}
                onPress={() => setIndex("user")}
                checkedIcon="dot-circle-o"
                uncheckedIcon="circle-o"
                checkedColor="#57A6A1"
                uncheckedColor="#57A6A1"
              />
            </View>
            <View
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
                gap: 10,
              }}
            >
              <Icon name="plumbing" size={30} color="#496989" />
              <Text
                style={{
                  fontSize: 20,
                  color: "#496989",
                  fontFamily: "Roboto-Italic",
                }}
              >
                Người cung cấp
              </Text>
              <Image
                source={provider_role}
                style={{ width: 150, height: 150, borderRadius: 12 }}
              />
              <CheckBox
                checked={selectedIndex === "provider"}
                onPress={() => setIndex("provider")}
                checkedIcon="dot-circle-o"
                uncheckedIcon="circle-o"
                checkedColor="#496989"
                uncheckedColor="#57A6A1"
              />
            </View>
          </View>
          <Button
            title="Tiếp tục"
            buttonStyle={{
              width: 250,
              height: 50,
              borderRadius: 12,
              backgroundColor: "#57A6A1",
              borderWidth: 1,
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
              margin: 20,
            }}
            onPress={() =>
              navigation.navigate("SignUpScreen", { role: `${selectedIndex}` })
            }
          ></Button>
          <Button
            title="Quay về"
            titleStyle={{
              color: "#57A6A1",
            }}
            buttonStyle={{
              width: 250,
              height: 50,
              borderRadius: 12,
              borderColor: "#57A6A1",
              borderWidth: 1,
              backgroundColor: "white",
              borderWidth: 1,

              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
              margin: 20,
            }}
            onPress={() => navigation.goBack()}
          ></Button>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ChooseRole;
