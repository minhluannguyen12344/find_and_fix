import { Text, View, Image, ScrollView, TextInput } from "react-native";
import React from "react";
import { useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { Button, Icon, Dialog } from "@rneui/themed";
import localHostEnv from "../config/localhost_env";
import * as ImagePicker from "expo-image-picker";
import Toast from "react-native-toast-message";
import { useNavigation } from "@react-navigation/native";
import { useFonts } from "expo-font";

const ProviderServices = (prop) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const navigation = useNavigation();
  const userInfor = useSelector((state) => state.user.userInfo);
  useEffect(() => {
    if (userInfor) {
      const id = userInfor._id;
      console.log("user", id);
    }
  }, [userInfor]);
  const userId = prop.userId;
  const showUpdateToast = () => {
    Toast.show({
      type: "success",
      visibilityTime: 800,
      text1: "Cập nhật dịch vụ thành công !",
    });
  };
  const showDeleteToast = () => {
    Toast.show({
      type: "success",
      visibilityTime: 800,
      text1: "Xóa dịch vụ thành công !",
    });
  };
  const [provideServices, setProviderServices] = useState([]);
  // set update state
  const [currentServices, setCurrentServices] = useState(null);
  const [showUpdateDialog, setShowUpdateDialog] = useState(false);
  const [servicesName, setServicesName] = useState("");
  const [servicesDescription, setServicesDescription] = useState("");
  const [servicesThumbnail, setServicesThumbnail] = useState(null);
  const [servicesPrice, setServicesPrice] = useState(0);
  const [servicesArea, setServicesArea] = useState("");
  const [servicesExperience, setServicesExperience] = useState("");
  const [error, setError] = useState({});
  const [reload, setReload] = useState(false);
  // set delete state
  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const [currentDeleteServices, setCurrentDeleteServices] = useState(null);
  // handle delete logic
  const deleteServices = (service) => {
    setCurrentDeleteServices(service);
    setShowDeleteDialog(!showDeleteDialog);
  };
  // handle update logic
  const updateComment = (service) => {
    setCurrentServices(service);
    setShowUpdateDialog(!showUpdateDialog);
  };
  useEffect(() => {
    if (currentServices) {
      const defaultThumbnail = `${localHostEnv}/${currentServices.thumbnail.replace(
        /\\/g,
        "/"
      )}`;
      setServicesName(currentServices.servicesName || "");
      setServicesPrice(currentServices.price || 0);
      setServicesDescription(currentServices.description || "");
      setServicesArea(currentServices.area || "");
      setServicesExperience(currentServices.exp || "");
      setServicesThumbnail(defaultThumbnail);
    }
  }, [currentServices]);
  useEffect(() => {
    if (currentDeleteServices) {
      setServicesName(currentDeleteServices.servicesName || "");
    }
  }, [currentDeleteServices]);
  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setServicesThumbnail(result.assets[0].uri);
    }
  };
  const validationUpdateService = () => {
    let newErrors = {};
    if (servicesName === "" || servicesName === null) {
      newErrors.servicesName = "Vui điền tên dich vụ";
    }
    if (servicesDescription === "") {
      newErrors.servicesDescription =
        "Vui lòng mô tả thông tin dịch vụ của bạn";
    }
    if (servicesPrice === 0) {
      newErrors.servicesPrice = "Vui lòng nhập giá dịch vụ";
    }
    if (servicesArea === "") {
      newErrors.servicesArea = "Vui lòng nhập địa chỉ dịch vụ";
    }
    if (servicesExperience === "") {
      newErrors.servicesExperience = "Vui lòng nhập kinh nghiệm";
    }
    if (servicesThumbnail === null) {
      newErrors.servicesThumbnail = "Vui lòng cập nhật ảnh dịch vụ";
    }
    setError(newErrors);
    return Object.keys(newErrors).length === 0;
  };
  const handleSubmitService = async () => {
    const formData = new FormData();
    formData.append("servicesName", servicesName);
    formData.append("description", servicesDescription);
    formData.append("price", servicesPrice);
    formData.append("area", servicesArea);
    formData.append("exp", servicesExperience);
    formData.append("thumbnail", {
      uri: servicesThumbnail,
      name: servicesThumbnail.split("/").pop(),
      type: "image",
    });
    console.log("formdata", formData);
    if (validationUpdateService()) {
      try {
        const response = await axios.put(
          `${localHostEnv}/services/${currentServices._id}`,
          formData
        );

        showUpdateToast();
        setShowUpdateDialog(false);
        setReload((prev) => !prev);
      } catch (e) {
        console.error("Failed to update services:", e);
      }
    }
  };
  // handle delete service
  const handleDeleteService = async () => {
    try {
      const response = await axios.delete(
        `${localHostEnv}/services/${currentDeleteServices._id}`
      );

      setShowDeleteDialog(false);
      showDeleteToast();
      setReload((prev) => !prev);
    } catch (e) {
      console.error("Failed to delete services:", e);
    }
  };
  // Get Provider services
  useEffect(() => {
    const getServices = async () => {
      try {
        const response = await axios.get(
          `${localHostEnv}/services/provider/${userId}`
        );
        console.log("services", response.data);
        setProviderServices(response.data);
      } catch (e) {
        console.error("Failed to fetch services data:", e);
      }
    };
    getServices();
  }, [reload]);

  return (
    <View
      style={{
        marginBottom: 100,
        width: "100%",
      }}
    >
      <View
        style={{
          width: "100%",
          transform: [{ translateX: 180 }],
        }}
      >
        <Button
          title="Làm mới danh sách"
          onPress={() => setReload((prev) => !prev)}
          buttonStyle={{
            backgroundColor: "#F0EBE3",

            width: 40,
            height: 40,
            gap: 10,
            borderRadius: 100,
            borderColor: "grey",
            borderWidth: 1,
          }}
        >
          <Icon name="reload" size={20} type="ionicon" color="#45474B" />
        </Button>
      </View>
      <View
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          gap: 20,
          marginBottom: 40,
          marginTop: 20,
          marginLeft: 20,
          marginRight: 20,
        }}
      >
        {provideServices.map((service, index) => (
          <View
            key={index}
            style={{
              display: "flex",
              justifyContent: "start",
              alignItems: "flex-start",
              flexDirection: "column",
              gap: 20,
              width: "100%",
              backgroundColor: "#FCF8F3",
              height: "auto",
              borderColor: "grey",
              borderWidth: 1,
              borderRadius: 12,
            }}
          >
            <Image
              source={{
                uri: `${localHostEnv}/${service.thumbnail.replace(/\\/g, "/")}`,
              }}
              style={{
                width: "100%",
                height: 200,
                objectFit: "cover",
                borderTopLeftRadius: 12,
                borderTopRightRadius: 12,
              }}
            />
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%",
                alignItems: "flex-start",
                padding: 10,
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  gap: 10,
                }}
              >
                <Text
                  style={{
                    color: "#496989",
                    fontFamily: "Roboto-BoldItalic",
                    fontSize: 20,
                  }}
                >
                  {service.servicesName}
                </Text>
                <View
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      color: "#FF4C4C",
                      fontFamily: "Roboto-BoldItalic",
                      fontSize: 18,
                    }}
                  >
                    {service.price}
                  </Text>
                  <Text
                    style={{
                      color: "#496989",
                      fontFamily: "Roboto-BoldItalic",
                      fontSize: 14,
                      textDecorationLine: "underline",
                    }}
                  >
                    đ
                  </Text>
                </View>

                <View
                  style={{
                    display: "flex",
                    justifyContent: "start",
                    alignItems: "center",
                    flexDirection: "row",
                  }}
                >
                  <Button
                    title="Xem chi tiết"
                    titleStyle={{ color: "white", fontFamily: "Roboto" }}
                    buttonStyle={{
                      width: "fit-content",
                      height: 40,
                      borderRadius: 8,
                      borderColor: "#57A6A1",
                      backgroundColor: "#4D869C",

                      borderWidth: 1,
                    }}
                    onPress={() => {
                      navigation.navigate("ProviderDetailServicesScreen", {
                        servicesId: service._id,
                        servicesName: service.servicesName,
                        servicesPrice: service.price,
                        servicesDescription: service.description,
                        servicesArea: service.area,
                        servicesExperience: service.exp,
                        servicesThumbnail: `${localHostEnv}/${service.thumbnail.replace(
                          /\\/g,
                          "/"
                        )}`,
                        providerId: service.createdBy,
                      });
                    }}
                  />
                </View>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  gap: 5,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",

                    fontSize: 20,
                    fontFamily: "Roboto-Bold",
                  }}
                >
                  Thao tác
                </Text>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    gap: 15,
                  }}
                >
                  <Icon
                    type="font-awesome-5"
                    name="edit"
                    size={25}
                    color="#4D4C7D"
                    onPress={() => updateComment(service)}
                  />
                  <Icon
                    name="delete"
                    size={25}
                    color="#EE4E4E"
                    onPress={() => deleteServices(service)}
                  />
                </View>
              </View>
            </View>
          </View>
        ))}
        <Dialog
          isVisible={showUpdateDialog}
          onBackdropPress={() => setShowUpdateDialog(false)}
          overlayStyle={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            direction: "column",
            gap: 10,
          }}
        >
          <ScrollView
            style={{
              width: "100%",
            }}
            showsVerticalScrollIndicator={false}
          >
            <Dialog.Title
              // title="Cập nhật dịch vụ"
              titleStyle={{
                color: "#57A6A1",
                fontSize: 24,
                fontWeight: "bold",
              }}
            />
            {currentServices && (
              <View
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  width: "100%",
                  gap: 20,
                }}
              >
                <View style={{ width: "100%", gap: 5 }}>
                  <Text
                    style={{
                      color: "#45474B",
                      fontSize: 16,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    Tên sản phẩm
                  </Text>
                  <TextInput
                    style={{
                      height: 50,
                      // borderColor: "#57A6A1",
                      // borderWidth: 1,
                      borderRadius: 12,
                      width: "100%",
                      color: "#496989",
                      backgroundColor: "#EEEEEE",
                      padding: 10,
                    }}
                    value={servicesName}
                    onChangeText={(text) => setServicesName(text)}
                  />
                  {error.servicesName && (
                    <Text
                      style={{
                        color: "red",
                        fontSize: 16,
                      }}
                    >
                      {error.servicesName}
                    </Text>
                  )}
                </View>
                <View style={{ width: "100%", gap: 5 }}>
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#45474B",
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    Mô tả dịch vụ
                  </Text>
                  <TextInput
                    style={{
                      height: 50,
                      borderRadius: 12,
                      width: "100%",
                      color: "grey",
                      color: "#496989",
                      backgroundColor: "#EEEEEE",
                      padding: 10,
                    }}
                    value={servicesDescription}
                    onChangeText={(text) => setServicesDescription(text)}
                    multiline={true}
                    numberOfLines={3}
                  />
                  {error.servicesDescription && (
                    <Text
                      style={{
                        color: "red",
                        fontSize: 16,
                      }}
                    >
                      {error.servicesDescription}
                    </Text>
                  )}
                </View>
                <View
                  style={{
                    width: "100%",
                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#45474B",
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    Giá tiền
                  </Text>
                  <TextInput
                    style={{
                      height: 50,
                      backgroundColor: "#EEEEEE",
                      color: "#496989",
                      borderRadius: 12,
                      width: "100%",

                      padding: 10,
                    }}
                    value={servicesPrice.toString()}
                    onChangeText={(text) => setServicesPrice(Number(text))}
                    keyboardType="numeric"
                  />
                  {error.servicesPrice && (
                    <Text
                      style={{
                        color: "red",
                        fontSize: 16,
                      }}
                    >
                      {error.price}
                    </Text>
                  )}
                </View>
                <View
                  style={{
                    width: "100%",
                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#45474B",
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    Địa chỉ
                  </Text>
                  <TextInput
                    style={{
                      height: 50,

                      backgroundColor: "#EEEEEE",
                      color: "#496989",
                      borderRadius: 12,
                      width: "100%",

                      padding: 10,
                    }}
                    value={servicesArea}
                    onChangeText={(text) => setServicesArea(text)}
                  />
                  {error.servicesArea && (
                    <Text
                      style={{
                        color: "red",
                        fontSize: 16,
                      }}
                    >
                      {error.servicesArea}
                    </Text>
                  )}
                </View>
                <View
                  style={{
                    width: "100%",
                    gap: 5,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#45474B",
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    Kinh nghiệm sửa chữa
                  </Text>
                  <TextInput
                    style={{
                      height: 50,
                      backgroundColor: "#EEEEEE",
                      borderRadius: 12,
                      width: "100%",
                      color: "#496989",

                      padding: 10,
                    }}
                    value={servicesExperience}
                    onChangeText={(text) => setServicesExperience(text)}
                  />
                  {error.servicesExperience && (
                    <Text
                      style={{
                        color: "red",
                        fontSize: 16,
                      }}
                    >
                      {error.servicesExperience}
                    </Text>
                  )}
                </View>
                <View
                  style={{
                    width: "100%",
                  }}
                >
                  {/* <Text
                    style={{
                      fontSize: 16,
                      color: "gray",
                      fontWeight: "bold",
                      marginBottom: 10,
                    }}
                  >
                    Ảnh bìa dịch vụ
                  </Text> */}
                  <Button
                    buttonStyle={{
                      backgroundColor: "white",
                      width: "100%",
                      borderColor: "#EEEEEE",

                      borderWidth: 1,
                      height: 50,
                      gap: 10,
                      borderRadius: 12,
                    }}
                    onPress={pickImage}
                    titleStyle={{
                      fontSize: 20,
                      fontWeight: "bold",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        fontFamily: "Roboto-Bold",
                        color: "#758694",
                      }}
                    >
                      {" "}
                      Cập nhật ảnh
                    </Text>
                    <Icon name="upload" size={20} color="#758694" />
                  </Button>
                  {error.servicesThumbnail && (
                    <Text
                      style={{
                        color: "red",
                        fontSize: 16,
                      }}
                    >
                      {error.servicesThumbnail}
                    </Text>
                  )}
                  {servicesThumbnail && (
                    <Image
                      source={{
                        uri: servicesThumbnail,
                      }}
                      style={{
                        marginTop: 20,
                        width: "100%",
                        height: 250,
                        objectFit: "cover",
                        borderRadius: 12,
                      }}
                    />
                  )}
                </View>
              </View>
            )}
            <Button
              title="Thay đổi"
              onPress={() => handleSubmitService()}
              buttonStyle={{
                backgroundColor: "#57A6A1",
                width: "100%",
                height: 50,
                borderRadius: 12,
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
                marginTop: 20,
              }}
              titleStyle={{
                fontSize: 20,
                fontWeight: "bold",
              }}
            />
            <Button
              title="quay về"
              buttonStyle={{
                backgroundColor: "white",
                marginTop: 20,
                width: "100%",
                height: 50,
                borderRadius: 12,
                borderColor: "#EE4E4E",
                borderWidth: 1,
              }}
              onPress={() => setShowUpdateDialog(false)}
              titleStyle={{
                fontSize: 20,
                fontFamily: "Roboto-Bold",
                color: "#EE4E4E",
              }}
            />
          </ScrollView>
        </Dialog>
        <Dialog
          isVisible={showDeleteDialog}
          onBackdropPress={() => setShowDeleteDialog(false)}
          overlayStyle={{
            width: 350,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",

            gap: 10,
            borderRadius: 12,
          }}
        >
          <Dialog.Title
            title="Xóa dịch vụ này"
            titleStyle={{
              color: "#EE4E4E",
              fontSize: 24,
              fontWeight: "bold",
            }}
          />
          {/* <Text>{currentDeleteServices.servicesName}</Text> */}
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 10,
            }}
          >
            <Button
              title="Xóa"
              onPress={() => handleDeleteService()}
              buttonStyle={{
                backgroundColor: "#EE4E4E",
                width: 150,
                height: 50,
                borderRadius: 12,
                marginTop: 20,
              }}
              titleStyle={{
                fontSize: 20,
                fontWeight: "bold",
              }}
            />
            <Button
              title="Hủy"
              buttonStyle={{
                backgroundColor: "white",
                marginTop: 20,
                width: 150,
                height: 50,
                borderRadius: 12,
                borderColor: "#4D869C",
                borderWidth: 1,
              }}
              onPress={() => setShowDeleteDialog(false)}
              titleStyle={{
                fontSize: 20,
                fontWeight: "bold",
                color: "#4D869C",
              }}
            />
          </View>
        </Dialog>
      </View>
    </View>
  );
};

export default ProviderServices;
