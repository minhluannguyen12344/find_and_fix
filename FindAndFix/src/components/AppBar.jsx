import { Text, View, Image } from "react-native";
import { Header, Icon } from "@rneui/themed";
import { useSelector } from "react-redux";
import localHostEnv from "../config/localhost_env";
import React from "react";
import { useNavigation } from "@react-navigation/native";
const AppBar = () => {
  const userInfo = useSelector((state) => state.user.userInfo);
  const imageUrl = `${localHostEnv}/${userInfo?.avatar.replace(/\\/g, "/")}`;
  const navigation = useNavigation();
  return (
    <Header
      backgroundColor="#57A6A1"
      containerStyle={{
        height: 100,
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
      }}
      leftComponent={
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "start",
            alignItems: "center",
            width: 300,
            gap: 10,
          }}
        >
          <Image
            source={{ uri: imageUrl }}
            style={{ width: 40, height: 40, borderRadius: 100 }}
          />
          <View
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "start",
              gap: 5,
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: 16,
                fontWeight: "bold",
                width: "auto",
                maxWidth: 200,
              }}
            >
              {userInfo?.fullname}
            </Text>
            <View
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "flex-start",
                flexDirection: "row",
                gap: 5,
              }}
            >
              <Icon
                name="map-marker-alt"
                size={15}
                type="font-awesome-5"
                color="#FF4C4C"
              />
              <Text
                style={{
                  color: "white",
                  fontSize: 15,

                  width: "auto",
                  maxWidth: 200,
                }}
              >
                : {userInfo?.address}
              </Text>
            </View>
          </View>
        </View>
      }
      rightComponent={
        userInfo?.role === "provider" ? (
          ""
        ) : (
          <Icon
            type="font-awnsome"
            name="search"
            color="white"
            size={35}
            onPress={() => navigation.navigate("SearchScreen")}
          />
        )
      }
    ></Header>
  );
};

export default AppBar;
