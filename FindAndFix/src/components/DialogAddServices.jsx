import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Image,
} from "react-native";
import { Header, Button, Dialog } from "@rneui/themed";
import * as ImagePicker from "expo-image-picker";
import React from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import localHostEnv from "../config/localhost_env";
import axios from "axios";
import { useFonts } from "expo-font";
import { Icon } from "@rneui/base";

const DialogAddServices = () => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const userInfo = useSelector((state) => state.user.userInfo);
  const [addServicesVisible, setAddServicesVisible] = useState(false);
  const [servicesName, setServicesName] = useState("");
  const [servicesDescription, setServicesDescription] = useState("");
  const [servicesThumbnail, setServicesThumbnail] = useState(null);
  const [servicesPrice, setServicesPrice] = useState(0);
  const [servicesArea, setServicesArea] = useState("");
  const [servicesExperience, setServicesExperience] = useState("");
  const [error, setError] = useState({});
  const createId = userInfo?._id;
  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setServicesThumbnail(result.assets[0].uri);
    }
  };

  const validationService = () => {
    let newErrors = {};
    if (servicesName === "") {
      newErrors.servicesName = "Vui điền tên dich vụ";
    }
    if (servicesDescription === "") {
      newErrors.servicesDescription =
        "Vui lòng mô tả thông tin dịch vụ của bạn";
    }

    if (servicesPrice === 0) {
      newErrors.servicesPrice = "Vui lòng nhập giá dịch vụ";
    }
    if (servicesArea === "") {
      newErrors.servicesArea = "Vui lòng nhập địa chỉ dịch vụ";
    }
    if (servicesExperience === "") {
      newErrors.servicesExperience = "Vui lòng nhập kinh nghiệm";
    }
    if (servicesThumbnail === null) {
      newErrors.servicesThumbnail = "Vui lòng cập nhật ảnh dịch vụ";
    }
    setError(newErrors);
    return Object.keys(newErrors).length === 0;
  };
  const handleSubmitService = async () => {
    if (validationService()) {
      const newServiceData = new FormData();
      newServiceData.append("servicesName", servicesName);
      newServiceData.append("description", servicesDescription);
      newServiceData.append("thumbnail", {
        uri: servicesThumbnail,
        type: "image",
        name: servicesThumbnail.split("/").pop(),
      });
      newServiceData.append("price", servicesPrice);
      newServiceData.append("area", servicesArea);
      newServiceData.append("exp", servicesExperience);
      newServiceData.append("createdBy", createId);
      console.log(newServiceData);
      try {
        const axiosResponse = await axios.post(
          `${localHostEnv}/services`,
          newServiceData,
          {
            headers: {
              Accept: "application/json",
            },
          }
        );
        setServicesName("");
        setServicesDescription("");
        setServicesArea("");
        setServicesExperience("");
        setServicesThumbnail(null);
        setServicesPrice(0);
        setAddServicesVisible(false);
      } catch (error) {
        if (error.response) {
          console.error(error.response.data.content);
        }
      }
    }
  };
  return (
    <View>
      <View
        style={{
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 10,
          },
          shadowOpacity: 0.1,
          shadowRadius: 3.84,
          elevation: 5,
          borderRadius: 10, // Match the button's borderRadius
        }}
      >
        <Button
          // title="Tạo Sản Phẩm"
          buttonStyle={{
            borderColor: "#57A6A1",
            // borderWidth: 1,
            backgroundColor: "#FCF8F3",

            width: 391,
            height: 68,
            borderRadius: 10,
          }}
          onPress={() => setAddServicesVisible(true)}
          titleStyle={{
            fontSize: 20,
            fontWeight: "bold",
          }}
        >
          <Text
            style={{
              fontFamily: "Roboto-Bold",
              color: "#45474B",
              fontSize: 25,
            }}
          >
            Tạo sản phẩm
          </Text>

          <Icon name="add" size={25} color="#45474B" />
        </Button>
      </View>

      <Dialog
        isVisible={addServicesVisible}
        onBackdropPress={() => setAddServicesVisible(false)}
        overlayStyle={{
          width: "100%",
          display: "flex",

          justifyContent: "center",
          alignItems: "center",
          direction: "column",
          gap: 10,
        }}
      >
        <ScrollView
          style={{
            width: "100%",
          }}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            gap: 15,
          }}
        >
          <Dialog.Title
            // title="Thông tin sản phẩm mới"
            titleStyle={{
              color: "#57A6A1",
              fontSize: 24,
              fontWeight: "bold",
            }}
          />
          <View
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              gap: 5,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 16,
              }}
            >
              Tên sản phẩm *
            </Text>
            <View
              style={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
                flexDirection: "row",
                backgroundColor: "#F5F7F8",
                borderRadius: 12,
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              <TextInput
                style={{
                  height: 50,
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                  width: "100%",
                }}
                onChangeText={(text) => setServicesName(text)}
              />
              <Icon name="edit" size={25} color="#4D4C7D" />
            </View>

            {error.servicesName && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {error.servicesName}
              </Text>
            )}
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              gap: 5,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 16,
              }}
            >
              Mô tả dịch vụ *
            </Text>
            <View
              style={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
                flexDirection: "row",
                backgroundColor: "#F5F7F8",
                borderRadius: 12,
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              <TextInput
                style={{
                  height: 50,
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                  width: "100%",
                }}
                onChangeText={(text) => setServicesDescription(text)}
              />
              <Icon name="edit" size={25} color="#4D4C7D" />
            </View>

            {error.servicesDescription && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {error.servicesDescription}
              </Text>
            )}
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              gap: 10,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 16,
              }}
            >
              Giá tiền *
            </Text>
            <View
              style={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
                flexDirection: "row",
                backgroundColor: "#F5F7F8",
                borderRadius: 12,
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              <TextInput
                style={{
                  height: 50,
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                  width: "100%",
                }}
                onChangeText={(text) => setServicesPrice(text)}
              />
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Roboto-Bold",
                  color: "#4D4C7D",
                  textDecorationLine: "underline",
                }}
              >
                đ
              </Text>
            </View>

            {error.servicesPrice && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {error.servicesPrice}
              </Text>
            )}
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              gap: 10,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 16,
              }}
            >
              Địa chỉ *
            </Text>
            <View
              style={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
                flexDirection: "row",
                backgroundColor: "#F5F7F8",
                borderRadius: 12,
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              <TextInput
                style={{
                  height: 50,
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                  width: "100%",
                }}
                onChangeText={(text) => setServicesArea(text)}
              />
              <Icon
                type="font-awesome-5"
                name="map-marker-alt"
                size={20}
                color="#4D4C7D"
              />
            </View>

            {error.servicesArea && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {error.servicesArea}
              </Text>
            )}
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              gap: 10,
            }}
          >
            <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 16,
              }}
            >
              Kinh nghiệm làm việc *
            </Text>
            <View
              style={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
                flexDirection: "row",
                backgroundColor: "#F5F7F8",
                borderRadius: 12,
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20,
              }}
            >
              <TextInput
                style={{
                  height: 50,
                  fontSize: 16,
                  fontFamily: "Roboto",
                  color: "#496989",
                  width: "100%",
                }}
                onChangeText={(text) => setServicesExperience(text)}
              />
              <Icon type="entypo" name="tools" size={20} color="#4D4C7D" />
            </View>

            {error.servicesExperience && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {error.servicesExperience}
              </Text>
            )}
          </View>
          <View
            style={{
              width: "100%",
            }}
          >
            {/* <Text
              style={{
                color: "#45474B",
                fontFamily: "Roboto-Bold",
                fontSize: 16,
                marginBottom: 10,
              }}
            >
              Ảnh bìa dịch vụ
            </Text> */}
            <Button
              buttonStyle={{
                backgroundColor: "white",
                width: "100%",
                borderColor: "#758694",

                borderWidth: 1,
                height: 50,
                gap: 10,
                borderRadius: 12,
              }}
              onPress={pickImage}
              titleStyle={{
                fontSize: 20,
                fontWeight: "bold",
              }}
            >
              <Text
                style={{
                  fontSize: 20,
                  fontFamily: "Roboto-Bold",
                  color: "#758694",
                }}
              >
                {" "}
                Ảnh dịch vụ *
              </Text>
              <Icon name="upload" size={20} color="#758694" />
            </Button>
            {error.servicesThumbnail && (
              <Text
                style={{
                  color: "red",
                  fontSize: 16,
                }}
              >
                {error.servicesThumbnail}
              </Text>
            )}
            {servicesThumbnail && (
              <Image
                source={{ uri: servicesThumbnail }}
                style={{
                  width: "100%",
                  height: 400,
                  objectFit: "cover",
                  borderRadius: 12,
                }}
              />
            )}
          </View>
          <View
            style={{
              width: "100%",
            }}
          >
            <Button
              title="Tạo sản phẩm"
              onPress={() => handleSubmitService()}
              buttonStyle={{
                backgroundColor: "#57A6A1",
                width: "100%",
                height: 50,
                borderRadius: 12,
                marginTop: 20,
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
              }}
              titleStyle={{
                fontSize: 20,
                fontWeight: "bold",
              }}
            />
          </View>
          <View
            style={{
              width: "100%",
            }}
          >
            <Button
              title="quay về"
              buttonStyle={{
                backgroundColor: "white",
                marginTop: 20,
                width: "100%",
                height: 50,
                borderRadius: 12,

                borderColor: "#EE4E4E",
                borderWidth: 1,
              }}
              onPress={() => setAddServicesVisible(false)}
              titleStyle={{
                fontSize: 20,
                fontFamily: "Roboto-Bold",
                color: "#EE4E4E",
              }}
            />
          </View>
        </ScrollView>
      </Dialog>
    </View>
  );
};

export default DialogAddServices;
