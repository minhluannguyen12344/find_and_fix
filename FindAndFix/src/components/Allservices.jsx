import React from "react";
import { Dimensions, Text, View, Image, ScrollView } from "react-native";
import { Header, Icon, Button } from "@rneui/themed";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import { useFonts } from "expo-font";

// local hostIp
import localHostEnv from "../config/localhost_env";
const Allservices = (prop) => {
  const userInfo = useSelector((state) => state.user.userInfo);
  const navigation = useNavigation();
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const servicesData = prop.servicesList;
  return (
    <ScrollView>
      <View
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          gap: 20,
          marginBottom: 40,
          marginTop: 20,
          marginLeft: 20,
          marginRight: 20,
        }}
      >
        {servicesData.map((service, index) => (
          <View
            key={index}
            style={{
              display: "flex",
              justifyContent: "start",
              alignItems: "center",
              flexDirection: "column",
              gap: 20,
              width: "100%",
              height: "auto",
              backgroundColor: "#FCF8F3",
              borderColor: "#7AB2B2",
              borderRadius: 12,
              // borderWidth: 1,
            }}
          >
            <Image
              source={{
                uri: `${localHostEnv}/${service.thumbnail.replace(/\\/g, "/")}`,
              }}
              style={{
                width: "100%",
                height: 200,
                borderTopLeftRadius: 12,
                borderTopRightRadius: 12,
              }}
            />
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                width: "100%",
                justifyContent: "space-between",
                gap: 8,
                alignItems: "flex-start",
              }}
            >
              <View
                style={{
                  flexDirection: "column",
                  display: "flex",
                  justifyContent: "center",
                  gap: 10,
                  paddingLeft: 10,
                  paddingBottom: 10,
                  alignItems: "flex-start",
                }}
              >
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    height: 30,
                    alignItems: "center",
                  }}
                >
                  {/* <Text
                    style={{
                      color: "#45474B",
                      fontFamily: "Roboto-Bold",
                      fontSize: 18,
                    }}
                  >
                    Tên dịch vụ:{" "}
                  </Text> */}
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{
                      color: "#496989",
                      fontFamily: "Roboto-BoldItalic",
                      fontSize: 20,
                      width: 130,
                    }}
                  >
                    {service.servicesName}
                  </Text>
                </View>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "start",
                  }}
                >
                  <Icon
                    name="map-marker-alt"
                    size={15}
                    type="font-awesome-5"
                    color="#FF4C4C"
                    containerStyle={{ marginTop: 5 }}
                  />
                  <Text
                    style={{
                      color: "#45474B",

                      fontSize: 18,
                    }}
                  >
                    :{" "}
                  </Text>
                  <Text
                    numberOfLines={2}
                    style={{
                      color: "#496989",
                      fontFamily: "Roboto-BoldItalic",
                      width: 200,
                      fontSize: 18,
                    }}
                  >
                    {service.area}
                  </Text>
                </View>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    height: 30,
                  }}
                >
                  <Text
                    style={{
                      color: "#45474B",
                      fontFamily: "Roboto-Bold",
                      fontSize: 18,
                    }}
                  >
                    Giá tiền:{" "}
                  </Text>
                  <Text
                    style={{
                      color: "#FF4C4C",
                      fontFamily: "Roboto-BoldItalic",
                      fontSize: 18,
                    }}
                  >
                    {service.price}
                  </Text>
                  <Text
                    style={{
                      color: "#496989",
                      fontFamily: "Roboto-BoldItalic",
                      fontSize: 14,
                      textDecorationLine: "underline",
                    }}
                  >
                    đ
                  </Text>
                </View>
                <Button
                  title="Chi tiết dịch vụ ->"
                  titleStyle={{ color: "white", fontFamily: "Roboto" }}
                  onPress={() => {
                    navigation.navigate("DetailServicesScreen", {
                      servicesId: service._id,
                      userId: userInfo._id,
                      servicesName: service.servicesName,
                      thumbnail: service.thumbnail,
                      price: service.price,
                      exp: service.exp,
                      description: service.description,
                      area: service.area,
                    });
                  }}
                  buttonStyle={{
                    width: "auto",
                    height: 40,
                    borderRadius: 4,

                    backgroundColor: "#4D869C",
                  }}
                />
              </View>

              <View
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column",
                  marginTop: 5,
                  gap: 10,
                  paddingRight: 20,
                }}
              >
                <Text
                  style={{
                    color: "#45474B",
                    fontFamily: "Roboto-BoldItalic",
                    fontSize: 16,
                    textDecorationLine: "underline",
                  }}
                >
                  Vị trí:
                </Text>
                <Icon
                  name="map"
                  size={30}
                  type="font-awesome-5"
                  color="#4D869C"
                  onPress={() =>
                    navigation.navigate("NavigateMap", {
                      area: service.area,
                    })
                  }
                />
              </View>
            </View>
          </View>
        ))}
      </View>
    </ScrollView>
  );
};

export default Allservices;
