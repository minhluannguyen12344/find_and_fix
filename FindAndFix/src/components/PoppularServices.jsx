import { Dimensions, Text, View, Image, ScrollView } from "react-native";
import { Header, Icon, Button } from "@rneui/themed";
import { useNavigation } from "@react-navigation/native";
import dataServices from "../fakedata/dataServices.json";
const PoppularServices = () => {
  const navigation = useNavigation();
  return (
    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          gap: 20,
          marginLeft: 20,
          marginTop: 20,
          marginBottom: 20,
        }}
      >
        {dataServices.servicesData.map((service, index) => (
          <View
            key={index}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row",
              gap: 20,
              width: 319,
              height: 120,
              borderColor: "#7AB2B2",
              borderRadius: 12,
              borderWidth: 1,
              padding: 10,
            }}
          >
            <Image
              source={{ uri: service.image }}
              style={{
                width: 100,
                height: 100,
                borderRadius: 12,
              }}
            />
            <View
              style={{
                display: "flex",
                height: 100,
                flexDirection: "column",
                justifyContent: "space-between",
                alignItems: "flex-start",
              }}
            >
              <Text
                style={{
                  color: "#4D869C",
                  fontWeight: "bold",
                  fontSize: 16,
                }}
              >
                {service.nameServices}
              </Text>
              <View
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-end",
                  flexDirection: "row",
                }}
              >
                <Text
                  style={{
                    color: "#57A6A1",
                    fontWeight: "bold",
                    fontSize: 16,
                  }}
                >
                  {service.price}
                </Text>
                <Text
                  style={{
                    color: "#808080",
                    fontWeight: 600,
                  }}
                >
                  {" "}
                  đ/gói
                </Text>
              </View>
              <View
                style={{
                  display: "flex",
                  alignItems: "center",
                  flexDirection: "row",
                }}
              ></View>
              <View
                style={{
                  display: "flex",
                  justifyContent: "start",
                  alignItems: "center",
                  flexDirection: "row",
                }}
              >
                <Button
                  title="Chi tiết dịch vụ"
                  titleStyle={{ color: "white", fontWeight: "bold" }}
                  buttonStyle={{
                    width: 180,
                    height: 40,
                    borderRadius: 12,
                    borderColor: "#57A6A1",
                    backgroundColor: "#4D869C",
                    borderWidth: 1,
                  }}
                  onPress={() => {
                    console.log(service);
                    navigation.navigate("DetailServicesScreen", {
                      serviceName: service.nameServices,
                      servicePrice: service.price,
                      serviceImage: service.image,
                      serviceDescription: service.description,
                      serviceId: service.id,
                      serviceProviderId: service.providerId,
                      serviceExperienced: service.experience,
                      servicesFeedback: service.feedback,
                    });
                  }}
                />
              </View>
            </View>
          </View>
        ))}
      </View>
    </ScrollView>
  );
};

export default PoppularServices;
