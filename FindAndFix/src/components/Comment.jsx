import { Text, View, TextInput, Image } from "react-native";
import React, { useState, useEffect } from "react";
import { Button, Icon, Dialog } from "@rneui/themed";
import { Rating } from "react-native-ratings";
import Toast from "react-native-toast-message";
import axios from "axios";
import localHostEnv from "../config/localhost_env";
import { useFonts } from "expo-font";

const Comment = (prop) => {
  const [fontsLoaded] = useFonts({
    Roboto: require("../../asset/font/Roboto-Regular.ttf"),
    "Roboto-Bold": require("../../asset/font/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("../../asset/font/Roboto-BoldItalic.ttf"),
    "Roboto-Italic": require("../../asset/font/Roboto-Italic.ttf"),
    "Roboto-Light": require("../../asset/font/Roboto-Light.ttf"),
    "Roboto-LightItalic": require("../../asset/font/Roboto-LightItalic.ttf"),
    "Roboto-Medium": require("../../asset/font/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("../../asset/font/Roboto-MediumItalic.ttf"),
    "Roboto-Thin": require("../../asset/font/Roboto-Thin.ttf"),
    "Roboto-ThinItalic": require("../../asset/font/Roboto-ThinItalic.ttf"),
  });
  if (!fontsLoaded) {
    return <ActivityIndicator size="large" />;
  }
  const service_id = prop.servicesId;
  const userId = prop.userId;

  const showToast = () => {
    Toast.show({
      type: "success",
      visibilityTime: 800,
      text1: "Bạn đã xóa đanh giá thành công",
    });
  };
  const successUpdateToast = () => {
    Toast.show({
      type: "success",
      visibilityTime: 800,
      text1: "Bạn đã cập nhật  giá thành công",
    });
  };
  // useState create comment
  const [userComment, setUserComment] = useState("");
  const [userRating, setUserRating] = useState(3);
  const [errors, setErrors] = useState({});
  // useState update comment
  const [userUpdateComment, setUserUpdateComment] = useState("");
  const [userUpdateRating, setUserUpdateRating] = useState(3);
  const [currentComment, setCurrentComment] = useState(null);
  const [isVisible, setIsVisible] = useState(false);
  // useState delete comment
  const [deleteComment, setDeleteComment] = useState(false);
  const [currentDeleteComment, setCurrentDeleteComment] = useState(null);
  const [isVisibleDelete, setIsVisibleDelete] = useState(false);
  const [commentInfo, setCommentInfo] = useState([]);
  const [commentSubmitted, setCommentSubmitted] = useState(false);
  // handle comment change
  const handleCommentChange = (text) => {
    setUserComment(text);
  };
  handleRatingChange = (value) => {
    setUserRating(value);
  };

  // handle update comment change
  const updateComment = (comment) => {
    setCurrentComment(comment);
    setIsVisible(!isVisible);
  };
  const handleUpdateCommentChange = (text) => {
    setUserUpdateComment(text);
  };
  handleUpdateRatingChange = (value) => {
    setUserUpdateRating(value);
  };
  // validation comment
  const validationComment = () => {
    let newErrors = {};
    if (userComment === "") {
      newErrors.userComment = "Vui lòng nhập đánh giá của bạn";
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };
  const handleSubmit = async () => {
    if (validationComment()) {
      try {
        const response = await axios.post(`${localHostEnv}/comment/`, {
          content: userComment,
          rating: userRating,
          userId: userId,
          servicesId: service_id,
        });
        console.log("Gửi đánh giá thành công", response.data);
      } catch (e) {
        console.error("Lỗi khi gửi đánh giá", e);
      }

      setUserComment("");
      setUserRating(3);
      setErrors({});
      setCommentSubmitted((prev) => !prev);
    }
  };
  const handleDeleteSubmit = async (id) => {
    try {
      const response = await axios.delete(`${localHostEnv}/comment/${id}`, {
        data: { servicesId: service_id },
      });
      console.log("xóa đánh giá thành công", response.data);
    } catch (e) {
      console.error("Lỗi khi xóa đánh giá", e);
    }
    showToast();
    setCommentSubmitted((prev) => !prev);
    deleteCommentdialog();
  };
  // validation update comment
  const validationUpdateComment = () => {
    let newErrors = {};
    if (!userUpdateComment) {
      newErrors.userUpdateComment = "Vui lòng nhập đánh giá của bạn";
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };
  const handleUpdateSubmit = async (id) => {
    if (validationUpdateComment()) {
      try {
        const response = await axios.put(`${localHostEnv}/comment/${id}`, {
          content: userUpdateComment,
          rating: userUpdateRating,
        });
      } catch (e) {
        console.error("Lỗi khi cập nhật đánh giá", e);
      }
      successUpdateToast();
      setUserUpdateComment("");
      setUserUpdateRating(3);
      setErrors({});
      setCurrentComment(null);
      setCommentSubmitted((prev) => !prev);
      updateComment();
    }
  };
  // Show dialog update comment

  // Show dialog delete comment
  const deleteCommentdialog = (comment) => {
    setCurrentDeleteComment(comment);
    setIsVisibleDelete(!isVisibleDelete);
  };

  useEffect(() => {
    const getCommentData = async () => {
      try {
        const response = await axios.get(
          `${localHostEnv}/comment/${service_id}`
        );
        setCommentInfo(response.data);
        console.log("commentInfo", response.data);
      } catch (e) {
        console.error("Load dữ liệu đánh giá lỗi", e);
      }
    };
    getCommentData();
  }, [commentSubmitted]);

  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-start",
        flexDirection: "column",
        gap: 20,
        marginBottom: 50,
      }}
    >
      <View
        style={{
          width: 374,
          height: "auto",
          borderRadius: 12,
          borderColor: "#EEEEEE",
          backgroundColor: "#F0EBE3",
          borderWidth: 1,
          padding: 10,
          display: "flex",
          gap: 20,
          justifyContent: "center",
          alignItems: "flex-start",
        }}
      >
        <TextInput
          style={{
            width: 350,
            fontSize: 18,
            fontFamily: "Roboto",
          }}
          placeholder="Nhập đánh giá của bạn..."
          onChangeText={handleCommentChange}
          multiline={true}
          numberOfLines={2}
          value={userComment}
        />
        <View
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            flexDirection: "row",
            width: "100%",
          }}
        >
          <Rating
            onFinishRating={this.handleRatingChange}
            tintColor="#F0EBE3"
            startingValue={userRating}
            imageSize={30}
          />
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#F0EBE3",
              width: 50,
              height: 40,
              borderRadius: 8,
              borderColor: "#45474B",
              // borderWidth: 1,
            }}
          >
            <Icon
              name="send"
              size={35}
              onPress={handleSubmit}
              color="#45474B"
            />
          </View>

          {/* <Button
            title="Gửi đánh giá"
            buttonStyle={{
              backgroundColor: "#57A6A1",
              width: 50,
              height: 50,
              borderRadius: 12,
              boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
            }}
            titleStyle={{
              fontSize: 20,
              fontWeight: "bold",
            }}
            
          /> */}
        </View>
        {errors.userComment && (
          <Text
            style={{
              color: "red",
              fontSize: 16,
            }}
          >
            {errors.userComment}
          </Text>
        )}
      </View>
      <Text
        style={{
          color: "#45474B",
          fontFamily: "Roboto-Bold",
          fontSize: 20,
        }}
      >
        Xem đánh giá :
      </Text>

      <View
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "flex-start",
          flexDirection: "column",
          gap: 20,
        }}
      >
        {commentInfo.map((comment, index) => (
          <View
            key={index}
            style={{
              width: 374,
              height: "auto",
              borderRadius: 12,
              borderColor: "#EEEEEE",
              backgroundColor: "#F0EBE3",
              borderWidth: 1,
              padding: 10,
              display: "flex",
              gap: 20,
              justifyContent: "center",
              alignItems: "flex-start",
            }}
          >
            <View
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "space-between",
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <View
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "row",
                }}
              >
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    borderRadius: 50,
                    marginRight: 10,
                  }}
                  alt="avatar"
                  source={{
                    uri: `${localHostEnv}/${comment.user.avatar.replace(
                      /\\/g,
                      "/"
                    )}`,
                  }}
                />
                <Text
                  style={{
                    color: "#496989",
                    fontFamily: "Roboto-BoldItalic",
                    fontSize: 20,
                  }}
                >
                  {comment.user.fullname}
                </Text>
              </View>
              {comment.user.userId === userId && (
                <View
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    flexDirection: "row",
                    gap: 20,
                  }}
                >
                  <Icon
                    type="font-awesome-5"
                    name="edit"
                    size={25}
                    color="#4D4C7D"
                    onPress={() => updateComment(comment)}
                  ></Icon>
                  <Icon
                    name="delete"
                    size={25}
                    color="#EE4E4E"
                    onPress={() => deleteCommentdialog(comment)}
                  ></Icon>
                </View>
              )}
            </View>
            <View
              style={{
                display: "flex",
                justifyContent: "start",
                alignItems: "flex-start",
                flexDirection: "column",
                gap: 10,
              }}
            >
              <Rating
                readonly
                startingValue={comment.rating}
                imageSize={20}
                tintColor="#F0EBE3"
              />

              <Text
                style={{
                  color: "grey",
                  fontFamily: "Roboto",
                  fontSize: 16,
                }}
              >
                {comment.content}
              </Text>
            </View>
          </View>
        ))}
        <Dialog
          isVisible={isVisible}
          onBackdropPress={() => setIsVisible(false)}
          overlayStyle={{
            borderRadius: 12,
            backgroundColor: "#F0EBE3",
          }}
        >
          <Dialog.Title
            title="Sửa đánh giá"
            titleStyle={{
              color: "#45474B",
              fontFamily: "Roboto-Bold",
              fontSize: 24,
            }}
          />
          {currentComment && (
            <View
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "flex-start",

                gap: 20,
              }}
            >
              <View
                style={{
                  borderRadius: 12,
                  borderColor: "grey",
                  borderWidth: 1,
                  display: "flex",
                  width: "100%",
                  height: "auto",
                  padding: 10,
                  justifyContent: "center",
                  alignItems: "flex-start",
                }}
              >
                <TextInput
                  multiline={true}
                  style={{
                    width: 250,
                    fontSize: 18,
                    color: "grey",
                    fontFamily: "Roboto",
                  }}
                  placeholder="Nhập đánh giá mới của bạn..."
                  onChangeText={handleUpdateCommentChange}
                  defaultValue={currentComment.content}
                  // value={userUpdateComment || }
                />
                <Rating
                  startingValue={currentComment.rating}
                  onFinishRating={this.handleUpdateRatingChange}
                  tintColor="#F0EBE3"
                  imageSize={30}
                />
              </View>

              <View
                style={{
                  display: "flex",
                  justifyContent: "center",
                  width: "100%",
                  alignItems: "center",
                  gap: 20,
                }}
              >
                <Button
                  title="Cập nhật"
                  onPress={() => handleUpdateSubmit(currentComment._id)}
                  buttonStyle={{
                    backgroundColor: "#4D4C7D",
                    width: 280,
                    height: 50,
                    borderRadius: 12,
                    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
                  }}
                  titleStyle={{
                    fontSize: 20,
                    fontFamily: "Roboto-Bold",
                  }}
                />
                <Button
                  title="quay về"
                  onPress={updateComment}
                  buttonStyle={{
                    backgroundColor: "white",
                    width: 280,
                    height: 50,
                    borderRadius: 12,
                  }}
                  titleStyle={{
                    fontSize: 20,
                    fontFamily: "Roboto-Bold",
                    color: "#FF7777",
                  }}
                />
              </View>
            </View>
          )}
        </Dialog>
        <Dialog
          isVisible={isVisibleDelete}
          onBackdropPress={() => setDeleteComment(false)}
          overlayStyle={{
            borderRadius: 12,
            backgroundColor: "#F0EBE3",
            width: 400,
            maxWidth: "100%",
          }}
        >
          <Dialog.Title
            title="Xóa đánh giá"
            titleStyle={{
              color: "#45474B",
              fontSize: 24,
              fontWeight: "bold",
            }}
          />
          {currentDeleteComment && (
            <View
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "start",
                gap: 20,
              }}
            >
              {/* <Text>{currentDeleteComment.name}</Text>
              <Text>{currentDeleteComment.content}</Text> */}
              <View
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                  alignItems: "center",
                  flexDirection: "row",
                  gap: 20,
                }}
              >
                <Button
                  title="Xóa"
                  onPress={() => handleDeleteSubmit(currentDeleteComment._id)}
                  buttonStyle={{
                    backgroundColor: "#EF5A6F",
                    width: 150,
                    height: 50,
                    borderRadius: 12,
                    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
                  }}
                  titleStyle={{
                    fontSize: 20,
                    fontFamily: "Roboto-Bold",
                  }}
                />
                <Button
                  title="quay về"
                  onPress={deleteCommentdialog}
                  buttonStyle={{
                    backgroundColor: "white",
                    width: 150,
                    height: 50,
                    borderRadius: 12,
                    borderColor: "#EF5A6F",
                    borderWidth: 1,
                  }}
                  titleStyle={{
                    fontSize: 20,
                    fontFamily: "Roboto-Bold",
                    color: "#EF5A6F",
                  }}
                />
              </View>
            </View>
          )}
        </Dialog>
      </View>
    </View>
  );
};

export default Comment;
