import { configureStore, createSlice } from "@reduxjs/toolkit";

// Initial state
const initialState = {
  userInfo: null,
};
// const orderInitialState = {
//   orderInfo: null,
// };

// Slice
const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    updateUserInfo: (state, action) => {
      state.userInfo = action.payload;
    },
  },
});
// const orderSlice = createSlice({
//   name: "order",
//   orderInitialState,
//   reducers: {
//     updateOrderInfo: (state, action) => {
//       state.orderInfo = action.payload;
//     },
//   },
// });
// Destructure and export the action
export const { updateUserInfo } = userSlice.actions;
// export const { updateOrderInfo } = orderSlice.actions;
// Configure and export the store
export default configureStore({
  reducer: {
    user: userSlice.reducer,
    // order: orderSlice.reducer,
  },
});
