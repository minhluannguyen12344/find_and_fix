//Navigation
import "react-native-gesture-handler";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
// import { navigationRef } from "./RootNavigation";
import { createNavigationContainerRef } from "@react-navigation/native";
import reduxStore from "./src/config/reduxStore";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect } from "react";
//Components library

import Toast from "react-native-toast-message";

import { Provider } from "react-redux";
import * as Linking from "expo-linking";
//Screens
import HomeScreen from "./src/screens/userscreen/HomeScreen";
import IntroScreen from "./src/screens/IntroScreen";
import LoginScreen from "./src/screens/LoginScreen";
import ChooseRole from "./src/screens/ChooseRole";
import SignUpScreen from "./src/screens/SignUpScreen";
import OtpVerifyScreen from "./src/screens/OtpVerifyScreen";
import UserMainLayout from "./src/screens/userscreen/UserMainLayout";
import UserHome from "./src/screens/userscreen/UserHome";
import DetailServicesScreen from "./src/screens/userscreen/DetailServicesScreen";
import ProviderDetailServicesScreen from "./src/screens/providerscreen/ProviderDetailServicesScreen";
import BookingScreen from "./src/screens/userscreen/BookingScreen";
import ProviderLayout from "./src/screens/providerscreen/ProviderLayout";
import NavigateMap from "./src/screens/NavigateMap";
import SearchScreen from "./src/screens/userscreen/SearchScreen";
const Stack = createNativeStackNavigator();
const navigationRef = createNavigationContainerRef();
const App = () => {
  useEffect(() => {
    const checkToken = async () => {
      const token = await AsyncStorage.getItem("userToken");
      const userId = await AsyncStorage.getItem("userId");
      const role = await AsyncStorage.getItem("userRole");
      console.log("store", token, userId, role);
      if (token && userId && role) {
        // Navigate to the main screen if token is present
        if (role === "user")
          navigationRef.current?.navigate("UserMainLayout", { userId: userId });
        else if (role === "provider")
          navigationRef.current?.navigate("ProviderLayout", { userId: userId });
      }
    };
    checkToken();
  }, []);
  const config = {
    animation: "spring",
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };
  const prefix = Linking.createURL("exp://192.168.1.8:8081/--/");
  const linking = {
    prefixes: [prefix],
    config: {
      screens: {
        UserMainLayout: {
          path: "UserMainLayout",
          screens: {
            Home: "Home",
            Order: "Order",
            Notification: "Notification",
            Info: "Info",
          },
        },
      },
    },
  };
  return (
    <Provider store={reduxStore} linking={linking}>
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator
          initialRouteName="IntroScreen"
          screenOptions={{
            gestureEnabled: false,
          }}
        >
          <Stack.Screen
            name="IntroScreen"
            component={IntroScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="LoginScreen"
            component={LoginScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="ChooseRole"
            component={ChooseRole}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="SignUpScreen"
            component={SignUpScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="OtpVerifyScreen"
            component={OtpVerifyScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="UserMainLayout"
            component={UserMainLayout}
            options={{
              gestureEnabled: false,
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="UserHome"
            component={UserHome}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="DetailServicesScreen"
            component={DetailServicesScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="BookingScreen"
            component={BookingScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="ProviderLayout"
            component={ProviderLayout}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="ProviderDetailServicesScreen"
            component={ProviderDetailServicesScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="NavigateMap"
            component={NavigateMap}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="HomeScreen"
            component={HomeScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name="SearchScreen"
            component={SearchScreen}
            options={{
              headerShown: false,
              transitionSpec: {
                open: config,
                close: config,
              },
              gestureEnabled: false,
            }}
          />
        </Stack.Navigator>
        <Toast />
      </NavigationContainer>
    </Provider>
  );
};
export default App;
